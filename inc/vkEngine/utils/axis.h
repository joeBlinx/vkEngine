//
// Created by stiven on 17-10-14.
//

#ifndef GLENGINE_AXIS_H
#define GLENGINE_AXIS_H

#include <glm/vec3.hpp>

namespace vkEngine {
    enum class Axis {
        WORLD, PARENT, OWN
    };
    enum class AxisVector{
        X, Y, Z
    };
    struct axis {
        glm::vec3 xAxis;
        glm::vec3 yAxis;
        glm::vec3 zAxis;
    };
    extern axis const worldAxis;
}
#endif //GLENGINE_AXIS_H
