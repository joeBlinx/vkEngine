//
// Created by stiven on 17-12-28.
//

#ifndef ALLPROJECT_LOG_HPP
#define ALLPROJECT_LOG_HPP

#include <string>
#include <iostream>
#include <vulkan/vulkan.h>

namespace vkEngine{

	template <class ...T>
	void displayError(T && ... errors){
		((std::cerr << errors<< ' '),...) << std::endl;
	}
	template <class ...T>
	void displayFatalError(T && ... errors){
		((std::cerr << errors<< ' '),...) << std::endl;
		throw ;
	}
	template<class ...Ts>
	void checkFatalResult(VkResult result, Ts && ... args ){
		if(result){
			displayFatalError(std::forward<Ts>(args)...);
		}
	}

	template <class ...Ts>
	void checkResult(VkResult result, Ts && ... args ){
		if(result){
			displayError(std::forward<Ts>(args)...);
		}
	}

}
#endif //ALLPROJECT_LOG_HPP
