//
// Created by stiven on 17-10-11.
//

#ifndef GLENGINE_MESHMANAGER_H
#define GLENGINE_MESHMANAGER_H

#include <vkEngine/manager/assetsList.h>
#include <string>
#include <vector>
#include <utility>
#include <vkEngine/vulkan/buffer.hpp>
#include <vkEngine/vulkan/initVulkan.hpp>

namespace vkEngine {
    struct mesh{
        using iterator = std::vector<std::pair<int, int>>::iterator;
        using const_iterator = std::vector<std::pair<int, int>>::const_iterator;
        vkEngine::Buffer verticesModel;
        std::vector<std::pair<int, int>> range;

        void bind()const{/* vao.bind();*/}
        const_iterator begin() const{ return range.begin();}
        iterator begin() { return range.begin();}
        const_iterator end() const{ return range.end();}
        iterator end() { return range.end();}
		mesh() = default;
        mesh(mesh &&) = default;

    };
    class MeshManager {
        static MeshManager manager;
        AssetsList<mesh> models;
        MeshManager() = default;
    public:
        static int get(std::string && path);
        static const mesh & get(int i);
        static void use(int i );
	    friend void vkEngine::InitVulkan::terminate();

    };
}
#endif //GLENGINE_MESHMANAGER_H
