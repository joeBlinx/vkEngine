//
// Created by stiven on 17-10-02.
//

#ifndef GLENGINE_ASSETSMANAGER_H
#define GLENGINE_ASSETSMANAGER_H

#include <string>
#include <map>
#include <vkEngine/parser/parser.h>
#include <glish/Vao.hpp>
#include <utility>
#include <vkEngine/manager/materialManager.h>
#include "meshManager.h"
namespace vkEngine {

    class AssetsManager {
        using mapMaterials = std::map<std::string, material>;
//        MaterialManager materialManager;
        int numberId = 0;
        std::string pathModel = "manager/model/";

        AssetsManager()  = default;

    public:
        static AssetsManager assetsManager;

        static int getMaterials(std::string && path);

        static int getModel(std::string && a);
        void changePath(std::string &&path);

        static int getTexture(std::string && path);


    };
}
#endif //GLENGINE_ASSETSMANAGER_H
