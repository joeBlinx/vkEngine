//
// Created by stiven on 17-09-25.
//

#ifndef GLENGINE_MOVABLEOBJECT_H
#define GLENGINE_MOVABLEOBJECT_H
#include <vkEngine/utils/axis.h>
#include <glm/vec3.hpp>

#include <glm/mat4x4.hpp>
namespace vkEngine {
    class MovableObject {
    protected:
        axis axe{worldAxis};
        glm::vec3 pos;
        glm::mat4 rotation;
        glm::vec3 scaling{1, 1, 1};
        glm::mat4 translation;
        glm::mat4 changeAxis;
        glm::mat4 transposeChangeAxis;
        glm::vec3 rotationCenter;
        glm::vec3 oldRotationCenter;
        bool moveWithBody = true;
        MovableObject(float x, float y, float z);
        MovableObject(glm::vec3 const &pos);
        MovableObject() = default;

        const axis &getAxis() const;
	    /**
	     * \brief Move the object along the axis given in parameter
	     * @param delta amount of movement
	     * @param axis axis you want to move along
	     */
        virtual void move(float delta, glm::vec3 const & axis);

    public:
        /**
         * \brief Rotate the object around the Xaxis
         * @param anglex Amount of rotation in radian
         * @param axisCoordinate determine which Xaxis to use, OWN, PARENT, WORLD
         */
        virtual void rotateX(float anglex, Axis axisCoordinate = Axis::OWN);
	    /**
		* \brief Rotate the object around the Yaxis
		* @param angley Amount of rotation in radian
		* @param axisCoordinate determine which Xaxis to use, OWN, PARENT, WORLD
		*/
        virtual void rotateY(float angley, Axis axisCoordinate = Axis::OWN);
	    /**
		* \brief Rotate the object around the Zaxis
		* @param anglez Amount of rotation in radian
		* @param axisCoordinate determine which Xaxis to use, OWN, PARENT, WORLD
		*/
        virtual void rotateZ(float anglez, Axis axisCoordinate = Axis::OWN);

		/**
		 * \brief Scale the object along x axis
		 * @param scalex ratio that will be mutiplty to actual scale
		 */
        void scaleX(float scalex);
		/**
		 * \brief Scale the object along y axis
		 * @param scaley ratio that will be mutiplty to actual scale
		 */
        void scaleY(float scaley);
		/**
		 * \brief Scale the object along z axis
		 * @param scalez ratio that will be mutiplty to actual scale
		 */
        void scaleZ(float scalez);
		/**
		 * \brief Scale the object along all axis
		 * @param scalex ratio that will be mutiplty to actual scale
		 */
        void scale(float scaleAll);
	    /**
	     * \cond PRIVATE
	     */
        virtual glm::mat4 getTransform() const;
		/**
		 * \endcond
		 * */
		/**
		 * \brief Get the position of the object
		 * @return pos
		 */
        const glm::vec3 &getPos() const;

		/**
		 * \brief Lock the center of rotation
		 * If it's lock, it will not move with the object
		 */
        void lockRotationCenter();

		/**
		 * \brief Unlock the rotation center
		 */
        void unlockRotationCenter();

        void changeRotationCenter(MovableObject const &object);

        void lastRotationCenter();

        virtual ~MovableObject() = 0;

    private:
        void rotate(glm::mat4 newRotate);
        glm::mat4 rotateOwnAxis(float value, glm::vec3 axis);
        void change(glm::mat4 const &newRotation);
        void move(glm::vec3 translate);

    };
}
#endif //GLENGINE_MOVABLEOBJECT_H
