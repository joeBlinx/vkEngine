//
// Created by stiven on 17-10-11.
//

#ifndef GLENGINE_MODEL_H
#define GLENGINE_MODEL_H

#include <vector>

namespace vkEngine {
    struct model3d {
        int meshId = -1; // meshManager
        int materials = -1; // materialManager
        std::vector<int> textures{-1};//TextureManager

    };
}
#endif //GLENGINE_MODEL_H
