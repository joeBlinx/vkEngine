//
// Created by stiven on 18-01-02.
//

#ifndef ALLPROJECT_ROOTNODE_HPP
#define ALLPROJECT_ROOTNODE_HPP

#include <vkEngine/scene/node.h>
namespace vkEngine {
	class Camera;

	class RootNode : public Node {
		bool firstCam = true;
		Camera const *camera = nullptr;
		static unsigned nextId;

		void assignNumber(Node *node) ;
		void update() override;

	public:
		using Node::Node;

		glm::mat4 getView();

		glm::mat4 getProj();

		void assignNumber();

		void changeCam(Camera const *newCam);

		~RootNode();
	};
}
#endif //ALLPROJECT_ROOTNODE_HPP
