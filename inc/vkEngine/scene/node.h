//
// Created by stiven on 17-10-02.
//

#ifndef GLENGINE_NODE_H
#define GLENGINE_NODE_H

#include <vkEngine/baseClass/movableObject.h>
#include <set>
#include <vkEngine/scene/model3d.h>
#include <vkEngine/display/camera.h>

namespace vkEngine {


	class RootNode;
    class Node : public MovableObject {
		friend class RootNode;
	    friend void update();


        static RootNode root;
	    Node * parent = nullptr;
	    unsigned number = 0;


    public:

        void rotateX(float anglex, Axis axisCoordinate = Axis::OWN) override;

        void rotateY(float angley, Axis axisCoordinate = Axis::OWN) override;

        void rotateZ(float anglez, Axis axisCoordinate = Axis::OWN) override;


        void moveX(float deltax, Axis axisCoordinate = Axis::OWN) ;

        void moveY(float deltay, Axis axisCoordinate = Axis::OWN) ;

        void moveZ(float deltaz, Axis axisCoordinate = Axis::OWN) ;

    private:


        using iterator = std::set<Node *>::iterator;
        using const_iterator = std::set<Node *>::const_iterator;
        model3d model;

	    void incrementNumber();
	    void decrementNumber(int numberToRemove);

    protected:

        Node() = default;
	    std::set<Node *> childs;
	    unsigned id;
	    using MovableObject::MovableObject;
	    virtual void update() = 0;

    public:
        Node(glm::vec3 const &pos);

        void addChild(Node *child);

        void removeChild(Node *child);

        iterator begin() ;

        const_iterator begin() const;

        iterator end() ;

        const_iterator end() const;

	    ~Node() override;

	    void addModel(std::string && a );

	    void addMaterial(std::string && path);

	    const model3d &getModel() const;

	    axis chooseCoordinate(Axis axisCoordinate);

	    unsigned size() const{ return number; }

	    unsigned int getId() const;

	    static void addToRoot(Node *child);

        static void removeFromRoot(Node *child);

        static RootNode * getRoot();

	    static Node * getRootAsNode();


    };




}
#endif //GLENGINE_NODE_H
