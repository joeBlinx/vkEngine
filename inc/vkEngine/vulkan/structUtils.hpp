//
// Created by stiven on 17-12-30.
//

#ifndef ALLPROJECT_STRUCTUTILS_HPP
#define ALLPROJECT_STRUCTUTILS_HPP


#include <vulkan/vulkan.h>
#include <vector>
#include <glm/glm.hpp>
#include <string>

namespace vkEngine{
	struct queueFamilyIndices {

		int graphicsFamily = -1;
		int presentFamily = -1;

		bool isComplete() {
			return graphicsFamily != -1 && presentFamily != -1;
		};
	};

	struct swapChainSupportDetail {
		VkSurfaceCapabilitiesKHR capabilities;
		std::vector<VkSurfaceFormatKHR> formats;
		std::vector<VkPresentModeKHR> presentModes;
	};

	struct shaderInfo{
		VkShaderStageFlagBits stage;
		std::string pathShader;
		std::vector<char> data;
	};

	struct uboCam{
		glm::mat4 view;
		glm::mat4 proj;
	};

	struct uboModel{
		glm::mat4 *model = nullptr;

	};

	struct vertices{
		glm::vec3 pos;
		glm::vec2 uv;
		glm::vec3 normals;
	};

	struct bufferData{
		VkBuffer buffer;
		VkDeviceMemory memory;
		VkDevice device;
	};

	struct textureData{
		VkImage image;
		VkImageView  view;
		VkDeviceMemory memory;
		VkSampler sampler;
		VkDevice device;
	};



}
#endif //ALLPROJECT_STRUCTUTILS_HPP
