//
// Created by stiven on 18-nullptr1-nullptr8.
//

#ifndef ALLPROJECT_TEXTURESAMPLER_HPP
#define ALLPROJECT_TEXTURESAMPLER_HPP

#include <vulkan/vulkan.h>
#include "structUtils.hpp"

namespace vkEngine {
	class TextureSampler {

		VkDeviceMemory memory = nullptr;
		VkSampler sampler = nullptr;
		VkImage image = nullptr;
		VkImageView view = nullptr;
		VkDevice device = nullptr;

		void emptyingTexture(TextureSampler && old);
	public:
		TextureSampler() = default;
		TextureSampler(textureData && data);

		TextureSampler(TextureSampler const &) = delete;
		TextureSampler&operator=(TextureSampler const &) = delete;

		TextureSampler(TextureSampler && old);
		TextureSampler&operator=(TextureSampler && old);

		VkSampler getSampler() const;

		VkImage getImage() const;

		VkImageView getImageView() const;

		void destroy();

		~TextureSampler();
	};
}


#endif //ALLPROJECT_TEXTURESAMPLER_HPP
