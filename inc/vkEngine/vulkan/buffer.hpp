//
// Created by stiven on 17-12-30.
//

#ifndef ALLPROJECT_VERTEX_HPP
#define ALLPROJECT_VERTEX_HPP


#include <vulkan/vulkan.h>
#include <string>
#include <vkEngine/vulkan/structUtils.hpp>
namespace vkEngine {
	class Buffer {

		VkBuffer buffer = 0;
		VkDeviceMemory memory = 0;
		VkDevice device = nullptr;

		void emptyingBuffer(Buffer &&oldVertex);
	public:

		Buffer() = default;

		Buffer(Buffer const &) = delete;

		Buffer &operator=(Buffer const &) = delete;

		Buffer(Buffer &&oldVertex);

		Buffer &operator=(Buffer &&oldVertex);

		Buffer(bufferData bufferInfo);

		VkBuffer const * getPtrBuffer()const { return &buffer;}

		VkDeviceMemory getMemory() const{return memory ;}

		operator VkBuffer() const{return buffer;}
		operator VkBuffer() {return buffer;}

		void destroy();

		~Buffer();
	};
}

#endif //ALLPROJECT_VERTEX_HPP
