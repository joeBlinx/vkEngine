//
// Created by stiven on 17-12-28.
//

#ifndef ALLPROJECT_INITVULKAN_HPP
#define ALLPROJECT_INITVULKAN_HPP

#include <vulkan/vulkan.h>
#include <array>
#include <vector>
#include <map>
#include <glm/glm.hpp>
#include <vkEngine/utils/log.hpp>
#include <memory>
#include "vkEngine/display/window.hpp"
#include "buffer.hpp"
#include "textureSampler.hpp"
#include <vkEngine/vulkan/structUtils.hpp>
#include <vkEngine/scene/node.h>

namespace vkEngine {

	class InitVulkan {

		unsigned numberTextureAllowed;


		static std::unique_ptr<InitVulkan> initVulkan;

		bool isInit = false;
		bool needRecord = true;
#ifndef NDEBUG
		bool enableValidationLayer = true;
#else
		bool enableValidationLayer = false;

#endif
		const std::array<const char *, 1> validationLayers{
				"VK_LAYER_LUNARG_standard_validation"
		};
		const std::vector<const char *> deviceExtensions{
				VK_KHR_SWAPCHAIN_EXTENSION_NAME
		};

		Window const &window;
		VkInstance instance;
		VkSurfaceKHR surface;

		//physical device
		queueFamilyIndices indices;
		swapChainSupportDetail swapChainDetail;
		VkPhysicalDeviceFeatures desiredFeature;
		VkPhysicalDevice physicalDevice;
		//logical device
		VkQueue graphicsQueue;
		VkQueue presentQueue;
		VkDevice device;
		//swapChain
		VkSwapchainKHR swapChain;
		std::vector<VkImage> swapChainImage;
		std::vector<VkImageView> swapChainImageViews;
		VkFormat swapChainFormat;
		VkExtent2D swapChainExtent;
		//render pass
		VkRenderPass renderpass;

		// commandpool
		VkCommandPool commandPool;

		//depth/Stencil Image
		VkImage depthImage;
		VkDeviceMemory depthImageMemory;
		VkImageView depthImageView;

		//framesbuffer
		std::vector<VkFramebuffer > framebuffers;

		//Pipeline
		VkPipelineCache pipelineCache;
		std::map<std::string, VkPipeline > pipelines;
		std::map<std::string, VkPipelineLayout > pipelinesLayout;

		//descriptor
		VkDescriptorPool descriptorPool;

		VkDescriptorSetLayout uboDescriptor;

		std::array<VkWriteDescriptorSet, 2> writesDescriptors{VkWriteDescriptorSet{},VkWriteDescriptorSet{}};
		VkDescriptorBufferInfo bufferInfo;
		std::vector<VkDescriptorImageInfo> imageInfos;
		VkDescriptorSet descriptorSet;
		VkDescriptorSetLayout uboDescriptorDynamic;
		VkDescriptorSet descriptorSetDynamic;


		//Uniform
		Buffer camBuffer;
		Buffer dynamicModelBuffer;
		uboModel dynamicUboModel{nullptr};
		size_t minUboAlignment ;

		//Vertex Buffer
		VkVertexInputBindingDescription vertexInputDescription;
		std::array<VkVertexInputAttributeDescription, 3> vertexAttributeDescription;

		//Command Buffer
		std::vector<VkCommandBuffer> commandBuffers;
		int lastMeshId = -1;

		//Semaphore for rendering
		VkSemaphore imageAvailableSemaphore;
		VkSemaphore renderFinishedSemaphore;


		std::vector<const char *> getRequiredExtensions();

		bool checkValidationLayersSupport();

		void createInstance();

		void createSurface();

		bool checkDeviceExtensionSupport(VkPhysicalDevice device);

		swapChainSupportDetail querySwapChainSupport(VkPhysicalDevice device);

		bool isDeviceSuitable(VkPhysicalDevice device);

		queueFamilyIndices findQueueFamilies(VkPhysicalDevice device);

		void choosePhysicalDevice();

		void createLogicalDevice();

		VkSurfaceFormatKHR chooseSwapChainSurfaceFormat();

		VkPresentModeKHR chooseSwapPresentMode();

		VkExtent2D chooseSwapExtent();

		void createImageView();

		void createSwapChain();

		void createRenderPass();

		void createCommandPool();

		VkFormat findDepthFormat();

		VkFormat findSupportedFormat(const std::vector<VkFormat> &candidatees, VkImageTiling tiling, VkFormatFeatureFlags features);

		void createImage(uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage,
		                 VkMemoryPropertyFlags properties, VkImage &image, VkDeviceMemory &imageMemory);

		uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties);

		VkImageView createImageView(VkImage image, VkFormat format, VkImageAspectFlags aspectFlags, uint32_t layerCount);

		void transitionImageLayout(VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout,
		                           int layerCount);

		VkCommandBuffer beginSingleTimeCommands();

		void endSingleTimeCommands(VkCommandBuffer commandBuffer);

		void createDepthImage();

		void createFrameBuffers();

		void createPipelineCache();

		bufferData createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties);

		void createUniformBuffer();

		void createDynamicBuffer();

		void createDescriptorSetLayout();

		void createDescriptorPool();

		void createDescriptorSet();

		void defineVertexInputSetting();

		void createPipelineLayout();

		VkPipelineShaderStageCreateInfo createShaderInfo(shaderInfo && shader);

		void copyBuffer(VkBuffer src, VkBuffer dest, VkDeviceSize size);

		void copyDataToBuffer(const void *data, unsigned size, VkDeviceMemory memory);

		void copyDataToBufferImage(VkBuffer buffer, VkImage image, int width, int height, int layer);

		void createCommandBuffer();

		/*void recordNode(const vkEngine::Node *child, int &id, VkCommandBuffer &commandBuffer,
		                VkPipelineLayout &pipelineLayout);
*/
		void createSemaphore();

		InitVulkan() = delete;

		InitVulkan(InitVulkan &&) = delete;

		InitVulkan(InitVulkan const &) = delete;

		InitVulkan& operator=(InitVulkan const &) = delete;

		InitVulkan& operator=(InitVulkan&&) = delete;

		InitVulkan(const Window &window, const VkPhysicalDeviceFeatures &desiredFeature,
				           unsigned numberTexture);

		void destroyDescriptor();

		void destroyUniformBuffer();

		void destroyPipeline();

		template<class ...Ts>
		static std::unique_ptr<InitVulkan> makeUnique(Ts  &&... args){
			return std::unique_ptr<InitVulkan>(new InitVulkan(std::forward<Ts>(args)...));
		}

		void drawPipeline();

		void recordCommandBuffer();

		void updateUni();
	public:

		template < class ...Shaders>
		void createGraphicsPipeline(Shaders && ...shader);

		//void createPipelineLayout(std::string const & key, );

		static void init(const Window &window, const VkPhysicalDeviceFeatures &desiredFeature, int numberTexture);

		static InitVulkan & getInstance();

		__attribute__ ((visibility ("hidden"))) VkShaderModule createShaderModule(std::vector<char> &&code);

		__attribute__ ((visibility ("hidden"))) Buffer createVertexBuffer(std::string &&path);

		__attribute__ ((visibility ("hidden"))) textureData createTexture(std::string const & path);

		__attribute__ ((visibility ("hidden"))) void updateDescriptorTexture();

		__attribute__ ((visibility ("hidden")))  void needNewRecord();

		void draw();

		bool shouldWindowClose();

		void terminate() ;

		virtual ~InitVulkan();


	};

	std::vector<char> readFile(const std::string &filename);
	template<class ...Shaders>
	void InitVulkan::createGraphicsPipeline(Shaders &&... shader) {

		size_t constexpr shaderNumber = sizeof...(shader);
		shaderInfo info[shaderNumber] = {std::forward<Shaders>(shader)...};
		std::array<VkPipelineShaderStageCreateInfo, shaderNumber> pipelinesShaderStages;
		for(unsigned i = 0 ; i < shaderNumber ; i++){
			pipelinesShaderStages[i] = createShaderInfo(std::move(info[i]));
		}

		VkPipelineVertexInputStateCreateInfo vertexInputInfo{};
		vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
		vertexInputInfo.vertexBindingDescriptionCount = 1;
		vertexInputInfo.pVertexBindingDescriptions = &vertexInputDescription;
		vertexInputInfo.vertexAttributeDescriptionCount = vertexAttributeDescription.size();
		vertexInputInfo.pVertexAttributeDescriptions = vertexAttributeDescription.data();


		VkPipelineInputAssemblyStateCreateInfo inputAssembly{};
		inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
		inputAssembly.primitiveRestartEnable = VK_FALSE;
		inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;


		VkViewport viewport{};
		viewport.x = 0.0f;
		viewport.y = 0.0f;
		viewport.width = (float) swapChainExtent.width;
		viewport.height = (float) swapChainExtent.height;
		viewport.minDepth = 0.0f;
		viewport.maxDepth = 1.0f;

		VkRect2D scissor{};
		scissor.offset= {0, 0};
		scissor.extent = swapChainExtent;

		VkPipelineViewportStateCreateInfo viewportState{};

		viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
		viewportState.pScissors = &scissor;
		viewportState.pViewports = &viewport;
		viewportState.scissorCount = 1;
		viewportState.viewportCount = 1;

		VkPipelineRasterizationStateCreateInfo rasterizationInfo{};
		rasterizationInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
		rasterizationInfo.depthClampEnable = VK_FALSE;
		rasterizationInfo.rasterizerDiscardEnable = VK_FALSE;
		rasterizationInfo.polygonMode = VK_POLYGON_MODE_FILL;
		rasterizationInfo.lineWidth = 1.0f;
		rasterizationInfo.cullMode = VK_CULL_MODE_NONE;
		rasterizationInfo.frontFace = VK_FRONT_FACE_CLOCKWISE;
		rasterizationInfo.depthBiasEnable = VK_FALSE;

		VkPipelineMultisampleStateCreateInfo multisampling = {};
		multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
		multisampling.sampleShadingEnable = VK_FALSE;
		multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
		multisampling.minSampleShading = 1.0f; // Optional
		multisampling.pSampleMask = nullptr; // Optional
		multisampling.alphaToCoverageEnable = VK_FALSE; // Optional
		multisampling.alphaToOneEnable = VK_FALSE; // Optional


		VkPipelineColorBlendAttachmentState colorBlendAttachment{};
		colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
		colorBlendAttachment.blendEnable = VK_FALSE;
		colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
		colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
		colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD; // Optional
		colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
		colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
		colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD; // Optional

		VkPipelineColorBlendStateCreateInfo colorBlending = {};
		colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
		colorBlending.logicOpEnable = VK_FALSE;
		colorBlending.logicOp = VK_LOGIC_OP_COPY; // Optional
		colorBlending.attachmentCount = 1;
		colorBlending.pAttachments = &colorBlendAttachment;
		colorBlending.blendConstants[0] = 0.0f; // Optional
		colorBlending.blendConstants[1] = 0.0f; // Optional
		colorBlending.blendConstants[2] = 0.0f; // Optional
		colorBlending.blendConstants[3] = 0.0f; // Optional


		VkPipelineDepthStencilStateCreateInfo depthStencil = {};
		depthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
		depthStencil.depthTestEnable = VK_TRUE;
		depthStencil.depthWriteEnable = VK_TRUE;
		depthStencil.depthCompareOp = VK_COMPARE_OP_LESS;
		depthStencil.depthBoundsTestEnable = VK_FALSE;
		depthStencil.minDepthBounds = 0.0f;
		depthStencil.maxDepthBounds = 1.0f;
		depthStencil.stencilTestEnable = VK_FALSE;


		VkGraphicsPipelineCreateInfo pipelineInfo {};
		pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
		pipelineInfo.stageCount = static_cast<uint32_t>(pipelinesShaderStages.size());
		pipelineInfo.pStages = pipelinesShaderStages.data();
		pipelineInfo.pVertexInputState = &vertexInputInfo;
		pipelineInfo.pInputAssemblyState = &inputAssembly;
		pipelineInfo.pViewportState = &viewportState;
		pipelineInfo.pRasterizationState = &rasterizationInfo;
		pipelineInfo.pMultisampleState = &multisampling;
		pipelineInfo.pColorBlendState = &colorBlending;
		pipelineInfo.pDepthStencilState = &depthStencil;
		pipelineInfo.layout = pipelinesLayout["basic"];
		pipelineInfo.renderPass = renderpass;
		pipelineInfo.subpass = 0;
		pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;

		pipelines.emplace(std::pair{"basic", VkPipeline{}});
		auto res = vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineInfo, nullptr, &pipelines["basic"]);
		checkFatalResult(res, "unable to create graphics pipeline");

		for(auto & shaderModule : pipelinesShaderStages){
			vkDestroyShaderModule(device, shaderModule.module, nullptr);
		}

		recordCommandBuffer();

	}

}
#endif //ALLPROJECT_INITVULKAN_HPP
