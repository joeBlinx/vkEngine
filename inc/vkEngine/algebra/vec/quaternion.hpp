//
// Created by darkblinx on 14/05/17.
//

#ifndef GLENGINE_QUATERNION_HPP
#define GLENGINE_QUATERNION_HPP

#include "vec4.hpp"

namespace vkEngine{
    struct quaternion : vec4<float> {
        using vec4<float> :: vec4;
        constexpr quaternion &operator*=(quaternion q);
    };

    constexpr quaternion operator*(quaternion q1, quaternion q2);
}
#endif //GLENGINE_QUATERNION_HPP
