//
// Created by darkblinx on 13/05/17.
//

#ifndef GLENGINE_UTILS_HPP
#define GLENGINE_UTILS_HPP

#include "vec2.hpp"
#include "vec3.hpp"
#include "vec4.hpp"

namespace vkEngine {


    template<class T, class U>
    constexpr auto dot(vec2<T> a, vec2<U> b){
        return a.x * b.x + a.y * b.y;
    };

    template<class T, class U>
    constexpr auto dot(vec3<T> a, vec3<U> b){
        return a.x * b.x + a.y * b.y + a.z * b.z;
    };
    template<class T, class U>
    constexpr auto dot(vec4<T> a, vec4<U> b){
        return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
    };

    template <class U>
    constexpr auto normalize( U a ){
        float norm = a.getNorm();
        return a / norm;
    }

    template <class U, class T>
    constexpr auto cross(vec3<U> a, vec3<T> b){

        auto x = a.y * b.z - a.z * b.y;
        auto y = a.z * b.x - a.x * b.z;
        auto z = a.x * b.y - a.y * b.x;
        return vec3<decltype(x)>{x, y, z};

    }
}
#endif //GLENGINE_UTILS_HPP
