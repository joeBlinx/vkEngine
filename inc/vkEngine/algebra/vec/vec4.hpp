//
// Created by darkblinx on 14/05/17.
//

#ifndef GLENGINE_VEC4_HPP
#define GLENGINE_VEC4_HPP

#include <type_traits>
#include <ostream>
#include <cmath>

namespace vkEngine {

    template<class T, class = std::enable_if_t<std::is_arithmetic<T>::value>>
    struct vec4 {
        T x = 0;
        T y = 0;
        T z = 0;
        T w = 0;

        /****************Constuctor and assignement**************************************/
        constexpr vec4() = default;

        constexpr vec4(T x) : x(x), y(x), z(x), w(x) {}

        constexpr vec4(T x, T y, T z, T w) : x(x), y(y), z(z), w(w) {}

        constexpr vec4(vec4 const &) = default;

        constexpr vec4(vec4 &&) = default;

        constexpr vec4 &operator=(vec4 const &) = default;

        constexpr vec4 &operator=(vec4 &&) = default;

        /********************************************************************************/

        constexpr float getNorm() {
            return std::sqrt(x * x + y * y + z * z + w*w);
        }

        constexpr vec4 operator+=(vec4<T> a) {
            x += a.x;
            y += a.y;
            z += a.z;
            w += a.w;
            return *this;
        }

        constexpr vec4 operator-=(vec4<T> a) {
            x -= a.x;
            y -= a.y;
            z -= a.z;
            w -= a.w;
            return *this;
        }

        constexpr vec4 operator*=(T a) {
            x *= a;
            y *= a;
            z *= a;
            w *= a;
            return *this;
        }

        constexpr vec4 operator/=(T a) {
            x /= a;
            y /= a;
            z /= a;
            w /= a;
            return *this;
        }
    };

    template<class T, class U>
    constexpr auto operator+(vec4<T> a, vec4<U> b) {
        return vec4<decltype(a.x + b.x)>{a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w};
    }

    template<class T, class U>
    constexpr auto operator*(vec4<T> a, U b) {
        return vec4<decltype(a.x * b)>{a.x * b, a.y * b, a.z * b, a.w * b};
    }

    template<class T, class U>
    constexpr auto operator-(vec4<T> a, vec4<U> b) {
        return vec4<decltype(a.x - b.x)>{a.x - b.x, a.y - b.y, a.z - b.z, a.w + b.w};
    }

    template<class T, class U>
    constexpr auto operator/(vec4<T> a, U b) {
        return vec4<decltype(a.x / b)>{a.x / b, a.y / b, a.z / b, a.w / b};
    }

    template<class T>
    std::ostream &operator<<(std::ostream &stream, vec4<T> a) {
        stream << "x: " << a.x << " y: " << a.y << " z: " << a.z << " w: " << a.w ;
        return stream;
    }

    using ivec4 = vec4<int>;
    using dvec4 = vec4<double>;
    using fvec4 = vec4<float>;
}
#endif //GLENGINE_VEC4_HPP
