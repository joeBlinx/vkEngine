//
// Created by darkblinx on 13/05/17.
//

#ifndef GLENGINE_VEC3_HPP_HPP
#define GLENGINE_VEC3_HPP_HPP

#include <type_traits>
#include <ostream>
#include <cmath>

namespace vkEngine{
    template<class T, class = std::enable_if_t<std::is_arithmetic<T>::value>>
    struct vec3 {
        T x = 0;
        T y = 0;
        T z = 0;

        /****************Constuctor and assignement**************************************/
        constexpr vec3() = default;
        constexpr vec3(T x):x(x), y(x), z(x){}
        constexpr vec3(T x, T y , T z):x(x),y(y), z(z){}
        constexpr vec3(vec3 const &) = default;
        constexpr vec3(vec3 &&) = default;
        constexpr vec3 &operator=(vec3 const &) = default;
        constexpr vec3 &operator=(vec3 &&) = default;
        /********************************************************************************/

        constexpr float getNorm(){

            return std::sqrt(x*x + y*y + z*z);
        }

        constexpr vec3 operator+=(vec3<T> a) {
            x += a.x;
            y += a.y;
            z += a.z;
            return *this;
        }
        constexpr vec3 operator-=(vec3<T> a) {
            x -= a.x;
            y -= a.y;
            z -= a.z;
            return *this;
        }
        constexpr vec3 operator*=(T a) {
            x *= a;
            y *= a;
            z *= a;
            return *this;
        }
        constexpr vec3 operator/=(T a) {
            x /= a;
            y /= a;
            z /= a;
            return *this;
        }
    };

    template <class T, class U>
    constexpr auto operator+(vec3<T> a, vec3<U> b) {
        return vec3<decltype(a.x + b.x)>{a.x + b.x, a.y + b.y, a.z + b.z};
    }

    template <class T, class U>
    constexpr auto operator*(vec3<T> a, U b) {
        return vec3<decltype(a.x * b)>{a.x * b, a.y * b, a.z * b};
    }

    template <class T, class U>
    constexpr auto operator-(vec3<T> a, vec3<U> b) {
        return vec3<decltype(a.x - b.x)>{a.x - b.x, a.y - b.y, a.z - b.z};
    }

    template <class T, class U>
    constexpr auto operator/(vec3<T> a, U b) {
        return vec3<decltype(a.x / b)>{a.x / b, a.y / b, a.z /b};
    }

    template <class T>
    std::ostream & operator <<(std::ostream & stream, vec3<T> a){
        stream << "x: " << a.x << " y: " << a.y << " z: " << a.z;
        return stream;
    }
    using ivec3= vec3<int>;
    using dvec3= vec3<double>;
    using fvec3= vec3<float>;
}
#endif //GLENGINE_VEC3_HPP_HPP
