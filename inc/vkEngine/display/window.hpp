//
// Created by stiven on 28/12/17.
//

#ifndef VKENGINE_WINDOW_HPP
#define VKENGINE_WINDOW_HPP

#include <iosfwd>
#include <GLFW/glfw3.h>
#include <vkEngine/event/keyHandler.hpp>
/*
 *
 * \brief
 * There is all you need to have for create a Window
 */

namespace vkEngine {
    struct windowSettings;

    struct Window {

	    windowSettings const & settings;
		/*!
		 * @brief create a window
		 * @param setting : settings for window
		 */
        Window(windowSettings const &setting);

	    /*!
	     *
	     * @return a pointer to a GLFWwindow
	     */
	    GLFWwindow *getWindow();
		/*!
		 *
		 * @return cast Window to a GLFWwindow pointer
		 */
        operator  GLFWwindow*()const { return m_window;}

        Window () = delete;

        Window(Window const &) = delete;

        Window operator=(Window const &) = delete;

        ~Window();


    private:
	    /*!
	     * \var : Pointer to GLFWwindow
	     */
	    GLFWwindow *m_window;
    };

}
#endif //GLENGINE_WINDOW_HPP
