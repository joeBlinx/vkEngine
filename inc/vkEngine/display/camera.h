//
// Created by stiven on 17-09-24.
//

#ifndef GLENGINE_CAMERA_H
#define GLENGINE_CAMERA_H

#include <glm/vec3.hpp>
#include <glm/detail/type_mat.hpp>
#include <glm/detail/type_mat4x4.hpp>
#include <vkEngine/baseClass/movableObject.h>

namespace vkEngine {
    class Camera : public MovableObject{
    protected:
        glm::vec3 lookAt;
        glm::vec3 up;
        float near;
        float far;
        glm::mat4 view;
        glm::mat4 projection;
        glm::mat4 camMatrix;

        void change();
        Camera(const glm::vec3 &pos, const glm::vec3 &lookAt, glm::mat4 projection);

    public:

        const glm::mat4 &getCamMatrix() const;

        const glm::mat4 &getView() const;

        const glm::mat4 getViewWhithoutTranslation() const;

        const glm::mat4 &getProjection() const;

        void zoom(float zoomToApply);

        void rotateX(float anglex, Axis axisCoordinate = Axis::OWN) override;

        void rotateY(float angley, Axis axisCoordinate = Axis::OWN) override;

        void rotateZ(float anglez, Axis axisCoordinate = Axis::OWN) override;

        virtual ~Camera() = 0 ;

    };

}
#endif //GLENGINE_CAMERA_H_H
