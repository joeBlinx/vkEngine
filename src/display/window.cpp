//
// Created by stiven on 28/12/17.
//

#include <stdexcept>
#include <glish/utils//log.hpp>
#include <vkEngine/display/window.hpp>
#include <vkEngine/display/windowSettings.hpp>
#include <GLFW/glfw3.h>
#include <vkEngine/event/keyEventHandler.hpp>

namespace vkEngine {

	void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
	{
		if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS){
			glfwSetWindowShouldClose(window, GLFW_TRUE);
		}
		if(action == GLFW_PRESS){
			KeyEventHandler::executePressedFunction(key);
		}
		if(action == GLFW_RELEASE){
			KeyEventHandler::executeReleasedFunction(key);
		}

	}


    Window::Window(windowSettings const &settings) : settings(settings) {
	    if(!glfwInit()){
		    std::cerr << "error while init glfw "<< std::endl;
	    }
	    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
	    m_window = glfwCreateWindow(settings.width, settings.height, settings.title.c_str(), nullptr, nullptr);

	    glfwSetKeyCallback(m_window, key_callback);

    }

	Window::~Window() {
		glfwDestroyWindow(m_window);
		glfwTerminate();
	}

	GLFWwindow *Window::getWindow() {
        return m_window;
    }




}