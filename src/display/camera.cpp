//
// Created by stiven on 17-09-24.
//

#include <vkEngine/display/camera.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>

#ifdef GLM_FORCE_RADIANS

#error "GLM_FORCE_RADIANS must not be declared"
#endif
namespace vkEngine {


	Camera::Camera(const glm::vec3 &pos, const glm::vec3 &lookAt, glm::mat4 projection) : MovableObject(pos),
	                                                                                      lookAt(lookAt) ,
	                                                                                      near(0.1),
	                                                                                      far(1000),
	                                                                                      up(glm::vec3{0, 1, 0}),
	                                                                                      view(glm::lookAt(
			                                                                                      pos,
			                                                                                      lookAt,
			                                                                                      up)),
	                                                                                      projection(projection),
	                                                                                      camMatrix(projection*view){

	}

	Camera::~Camera(){

	}

    const glm::mat4 &Camera::getCamMatrix() const {
        return camMatrix;
    }

    void Camera::zoom(float zoomToApply) {
        glm::vec3 direction = glm::normalize(lookAt - pos);
        pos += zoomToApply * direction;
        change();
    }

    void Camera::change() {


        view = glm::lookAt(pos, lookAt , up);
//        projection = glm::perspective(fov,
                                     // aspect,
                                      //near, far);
        camMatrix = (projection * view );

    }

// TODO : Camera must rotate in his own axis
    void Camera::rotateX(float anglex, Axis axisCoordinate) {
        pos = glm::rotateX(pos, anglex);
        up = glm::rotateX(up, anglex);
        axe.yAxis = glm::rotateX(axe.yAxis, anglex);
        axe.zAxis = glm::rotateX(axe.zAxis, anglex);
        change();

    }

    void Camera::rotateY(float angley, Axis axisCoordinate) {
        pos = glm::rotateY(pos, angley);
        up = glm::rotateY(up, angley);

        axe.xAxis = glm::rotateY(axe.xAxis, angley);
        axe.zAxis = glm::rotateY(axe.zAxis, angley);
        change();

    }

    void Camera::rotateZ(float anglez, Axis axisCoordinate) {
        pos = glm::rotateZ(pos, anglez);
	    up = glm::rotateZ(up, anglez);
	    axe.yAxis = glm::rotateZ(axe.yAxis, anglez);
        axe.xAxis = glm::rotateZ(axe.xAxis, anglez);
        change();

    }

	const glm::mat4 &Camera::getView() const {
		return view;
	}

	const glm::mat4 &Camera::getProjection() const {
		return projection;
	}

	const glm::mat4 Camera::getViewWhithoutTranslation() const {
		return glm::mat4(glm::mat3(view));
	}


}