//
// Created by stiven on 18-02-23.
//

#include <glm/gtc/matrix_transform.hpp>
#include "vkEngine/display/perspectiveCamera.hpp"

namespace vkEngine {
	PerspectiveCamera::PerspectiveCamera(const glm::vec3 &pos, const glm::vec3 &lookAt, float aspect) : Camera(pos,
	                                                                                                           lookAt,
	                                                                                                           glm::perspective(
				                                                                                                           70.0f,
				                                                                                                           aspect,
				                                                                                                           0.1f,
				                                                                                                           1000.0f)),
	                                                                                                    aspect(aspect),
	                                                                                                    fov(70.0) {


	}
}