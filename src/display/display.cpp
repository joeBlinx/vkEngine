//
// Created by stiven on 17-10-11.
//
#include <vkEngine/scene/node.h>
#include <vkEngine/manager/meshManager.h>
#include <vkEngine/display/display.h>
#include <vkEngine/display/camera.h>
#include <inc/vkEngine/manager/textureManager.h>
#include <vkEngine/utils/utils.h>

namespace vkEngine {

	void update(){
		glfwPollEvents();
		forEachNode([](vkEngine::Node * node){
			node->update();
		});
	}
}