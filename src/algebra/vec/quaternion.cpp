//
// Created by darkblinx on 27/06/17.
//
#include <algebra/vec/quaternion.hpp>

namespace vkEngine {

    constexpr quaternion operator*(quaternion q1, quaternion q2) {
        float x = q1.w * q2.w - q1.x * q2.x - q1.y * q2.y - q1.z * q2.z;
        float y = q1.w * q2.x + q1.x * q2.w + q1.y * q2.z - q1.z * q2.y;
        float z = q1.w * q2.y - q1.x * q2.z + q1.y * q2.w + q1.z * q2.x;
        float w = q1.w * q2.z + q1.x * q2.y - q1.y * q2.x + q1.z * q2.w;

        return quaternion{x, y, z, w};
    }

    constexpr quaternion &quaternion::operator*=(quaternion q) {
         x = w * q.w - x * q.x - y * q.y - z * q.z;
         y = w * q.x + x * q.w + y * q.z - z * q.y;
         z = w * q.y - x * q.z + y * q.w + z * q.x;
         w = w * q.z + x * q.y - y * q.x + z * q.w;
        return *this;
    }
}