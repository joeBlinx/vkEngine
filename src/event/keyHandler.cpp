//
// Created by stiven on 18-01-20.
//

#include <fstream>
#include <utils/stringUtil.h>
#include <vkEngine/event/keyHandler.hpp>
#include <vector>
#include <GLFW/glfw3.h>
#include <iostream>

namespace vkEngine {

/***
 *
 * @param configFile path to configFile
 *
 * content of configFile must be
 *
 * Forward = w, z
 */
	std::map<std::string, int> const keyBind{
			{"left_ctrl",   GLFW_KEY_LEFT_CONTROL},
			{"right_ctrl",  GLFW_KEY_RIGHT_CONTROL},
			{"left_shift",  GLFW_KEY_LEFT_SHIFT},
			{"right_shift", GLFW_KEY_RIGHT_SHIFT},
			{"space",       GLFW_KEY_SPACE},
			{"up",          GLFW_KEY_UP},
			{"down",        GLFW_KEY_DOWN},
			{"right",       GLFW_KEY_RIGHT},
			{"left",        GLFW_KEY_LEFT},
			{"escape",      GLFW_KEY_ESCAPE},
			{"tab",         GLFW_KEY_TAB},
			{"left_alt",    GLFW_KEY_LEFT_ALT},
			{"right_alt",   GLFW_KEY_RIGHT_ALT},
			{"enter",       GLFW_KEY_ENTER}
	};

	void assign(int & value, std::string strValue){
		strValue = utils::removeChar(std::move(strValue), ' ');
		if(strValue.size() > 1){
			strValue = utils::toLowerCase(std::move(strValue));
			try {
				value = keyBind.at(strValue);
			}catch (std::out_of_range const & e){
				value = -1;
				std::cerr << strValue << " is not a valid value" << std::endl;
			}
		}else{
			strValue = utils::toUpperCase(std::move(strValue));
			value = strValue[0];
		}
	}
	void KeyHandler::mapKey(std::string &&configFile) {
		using namespace utils;
		std::ifstream file{configFile};
		if(!file.is_open()){
			std::cerr<< "file "<< configFile << " hasn't been find" <<std::endl;
			return;
		}
		unsigned constexpr size = 100;
		char line[size] = {};
		std::string linestr;
		while (true) {

			typeMap newPair;
			auto & [key, pair] = newPair;
			auto & [value1, value2] = pair;
			file.getline(line, size);

			linestr = line;
			if(linestr.empty()){
				continue;
			}
			std::vector<std::string> splits = utils::split(linestr, '=');
			key = removeChar(splits[0], ' ');
			if(splits.size() < 2){
				std::cerr << "there is no value with the key "<< key << ".Make sure you haven't forgot to add one" << std::endl;
				value1 = -1;
				value2 = -1;
			}else {
				splits = split(splits[1], ',');
				assign(value1, splits[0]);

				if (splits.size() > 1) {
					assign(value2, splits[1]);
				} else {
					value2 = -1;
				};
			}
			keys.emplace(std::move(newPair));
			if (file.eof()) {
				break;
			}
		}
	}

	KeyHandler::iterator KeyHandler::begin() {
		return keys.begin();
	}

	KeyHandler::const_iterator KeyHandler::begin() const {
		return keys.begin();
	}

	KeyHandler::iterator KeyHandler::end() {
		return keys.end();
	}

	KeyHandler::const_iterator KeyHandler::end() const {
		return keys.end();
	}



	std::pair<int, int>  & KeyHandler::operator[](std::string const & key) {
		return keys[key];
	}

}