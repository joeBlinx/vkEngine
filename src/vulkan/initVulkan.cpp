//
// Created by stiven on 17-12-28.
//

#include <vkEngine/vulkan/initVulkan.hpp>
#include <iostream>
#include <cstring>
#include <vector>
#include <vkEngine/display/windowSettings.hpp>
#include <utils/log.hpp>
#include <set>
#include <fstream>
#include <vkEngine/parser/parser.h>
#include <vkEngine/vulkan/buffer.hpp>
#include <vkEngine/scene/node.h>
#include <vkEngine/manager/meshManager.h>
#include <glm/gtc/type_ptr.inl>
#include <scene/rootNode.hpp>
#include <vkEngine/display/perspectiveCamera.hpp>
#define  STB_IMAGE_IMPLEMENTATION
#include <vkEngine/vulkan/stb_image.h>
#include <vkEngine/manager/textureManager.h>

namespace vkEngine {
	std::unique_ptr<InitVulkan> InitVulkan::initVulkan;

	std::vector<char> readFile(const std::string &filename) {
		std::ifstream file(filename, std::ios::ate | std::ios::binary);

		if (!file.is_open()) {
			throw std::runtime_error("failed to open file!");
		}
		size_t fileSize = (size_t) file.tellg();
		std::vector<char> buffer(fileSize);
		file.seekg(0);
		file.read(buffer.data(), fileSize);
		file.close();

		return buffer;
	}

	[[maybe_unused]]  static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
			[[maybe_unused]] VkDebugReportFlagsEXT flags,
			[[maybe_unused]] VkDebugReportObjectTypeEXT objType,
			[[maybe_unused]] uint64_t obj,
			[[maybe_unused]] size_t location,
			[[maybe_unused]] int32_t code,
			[[maybe_unused]] const char *layerPrefix,
			[[maybe_unused]] const char *msg,
			[[maybe_unused]] void *userData) {

		std::cerr << "validation layer:" << msg << std::endl;

		return VK_FALSE;
	}

	bool InitVulkan::checkValidationLayersSupport() {

		uint32_t layerCount;
		vkEnumerateInstanceLayerProperties(&layerCount, nullptr);
		std::vector<VkLayerProperties> layers(layerCount);
		vkEnumerateInstanceLayerProperties(&layerCount, layers.data());

		for (auto validation : validationLayers) {
			for (auto layer : layers) {
				if (!strcmp(layer.layerName, validation)) {
					return true;
				}
			}
		}
		return false;
	}

	std::vector<const char *> InitVulkan::getRequiredExtensions() {
		std::vector<const char *> extensions;

		unsigned int glfwExtensionCount = 0;
		const char **glfwExtensions;
		glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

		for (unsigned int i = 0; i < glfwExtensionCount; i++) {
			extensions.push_back(glfwExtensions[i]);
		}
		if (enableValidationLayer) {
			extensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
		}

		return extensions;
	}

	void InitVulkan::createInstance() {
		if (enableValidationLayer && !checkValidationLayersSupport()) {
			displayError("Validation not supported, it will be disabled");
			enableValidationLayer = false;
		}

		VkApplicationInfo app{};
		app.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
		app.apiVersion = VK_MAKE_VERSION(window.settings.majorVersion, window.settings.minorVersion, 0);
		app.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
		app.engineVersion = VK_MAKE_VERSION(1, 0, 0);
		app.pEngineName = nullptr;
		app.pNext = nullptr;

		VkInstanceCreateInfo instanceInfo{};
		instanceInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		instanceInfo.pApplicationInfo = &app;



		//*L'extension dont a besoin vulkan pour communiquer avec la fenetre */
		auto extensions = getRequiredExtensions();
		instanceInfo.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
		instanceInfo.ppEnabledExtensionNames = extensions.data();
		instanceInfo.enabledLayerCount = 0;

		if (enableValidationLayer) {
			instanceInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
			instanceInfo.ppEnabledLayerNames = validationLayers.data();
		}


		VkResult result = vkCreateInstance(&instanceInfo, nullptr, &instance);
		if (result != VK_SUCCESS) {
			displayFatalError("failed to create instance!");
		}
	}

	void InitVulkan::createSurface() {
		if (glfwCreateWindowSurface(instance, window, nullptr, &surface) != VK_SUCCESS) {
			displayFatalError("failed to create window surface!");
		}
	}


// check if we can create a swapchain whose suit our need
	//here presentation mode and surface format
	swapChainSupportDetail InitVulkan::querySwapChainSupport(VkPhysicalDevice device) {
		swapChainSupportDetail details;
		// surface capabilities min/max number of image
		vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &details.capabilities);

		// surface format available pixel and color space
		uint32_t countFormat;
		vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &countFormat, nullptr);
		if (countFormat != 0) {
			details.formats.resize(countFormat);
			vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &countFormat, details.formats.data());
		}

		// presentaton mode
		uint32_t presentModeCount;
		vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, nullptr);

		if (presentModeCount != 0) {
			details.presentModes.resize(presentModeCount);
			vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, details.presentModes.data());
		}

		return details;
	}


	bool InitVulkan::checkDeviceExtensionSupport(VkPhysicalDevice device) {
		uint32_t count;
		vkEnumerateDeviceExtensionProperties(device, nullptr, &count, nullptr);
		std::vector<VkExtensionProperties> properties(count);
		vkEnumerateDeviceExtensionProperties(device, nullptr, &count, properties.data());

		std::set<std::string> required(std::begin(deviceExtensions), std::end(deviceExtensions));

		for (const auto &extension : properties) {
			required.erase(extension.extensionName);
		}

		return required.empty();
	}

	bool InitVulkan::isDeviceSuitable(VkPhysicalDevice device) {

		queueFamilyIndices indices = findQueueFamilies(device);

		bool extensionSupported = checkDeviceExtensionSupport(device);
		VkPhysicalDeviceFeatures supportedFeatures{};
		vkGetPhysicalDeviceFeatures(device, &supportedFeatures);
		bool swapChainAdequate = false;
		if (extensionSupported) {
			swapChainDetail = querySwapChainSupport(device);
			swapChainAdequate = !swapChainDetail.formats.empty() && !swapChainDetail.presentModes.empty();

		}
		// transform struct in tab to iterate over all bool
		bool checkFeature = true;
		size_t sizeDeviceFeature = sizeof(VkPhysicalDeviceFeatures);
		auto *supportedFeatureinTab =  (bool*)&supportedFeatures;

		auto *desiredFeatureInTab = (bool*)&desiredFeature;
		for (unsigned i = 0; i < sizeDeviceFeature / sizeof(VkBool32); i++) {
			if (!supportedFeatureinTab[i] && desiredFeatureInTab[i]) {
				checkFeature = false;
				break;
			}
		}

		return indices.isComplete() && extensionSupported && swapChainAdequate && checkFeature;

	}

//check if we can find the queuefamilies we need
	//For now only graphics and presentation
	//TODO: add computing queue
	queueFamilyIndices InitVulkan::findQueueFamilies(VkPhysicalDevice device) {
		queueFamilyIndices indices;
		uint32_t queueFamilyCount = 0;
		vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);
		std::vector<VkQueueFamilyProperties> queuesFamilies(queueFamilyCount);
		vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queuesFamilies.data());
		int i = 0;
		VkBool32 presentSupport;
		for (auto &queueFamily : queuesFamilies) {
			if (queueFamily.queueCount > 0 && queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
				indices.graphicsFamily = i;
			}
			presentSupport = VK_FALSE;
			vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &presentSupport);
			if (queueFamily.queueCount > 0 && presentSupport) {
				indices.presentFamily = i;
			}

			if (indices.isComplete()) {
				break;
			}
			i++;


		}
		return indices;
	}

	void InitVulkan::choosePhysicalDevice() {
		uint32_t count;
		vkEnumeratePhysicalDevices(instance, &count, nullptr);
		std::vector<VkPhysicalDevice> devices(count);
		vkEnumeratePhysicalDevices(instance, &count, devices.data());
		// looking for device that suit our need
		for (auto physDevice : devices) {
			if (isDeviceSuitable(physDevice)) {
				physicalDevice = physDevice;
				indices = findQueueFamilies(physDevice);
				break;
			}
		}
	}

	void InitVulkan::createLogicalDevice() {
		std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
		std::set<int> uniqueQueueFamilies = {indices.graphicsFamily, indices.presentFamily};

		float queuePriority = 1.0f;
		for (int queueFamily : uniqueQueueFamilies) {
			VkDeviceQueueCreateInfo queueCreateInfo = {};
			queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
			queueCreateInfo.queueFamilyIndex = static_cast<uint32_t>(queueFamily);
			queueCreateInfo.queueCount = 1;
			queueCreateInfo.pQueuePriorities = &queuePriority;
			queueCreateInfos.push_back(queueCreateInfo);
		}


		VkDeviceCreateInfo deviceCreateInfo{};
		deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
		deviceCreateInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
		deviceCreateInfo.pQueueCreateInfos = queueCreateInfos.data();

		deviceCreateInfo.pEnabledFeatures = &desiredFeature;
		deviceCreateInfo.enabledExtensionCount = static_cast<uint32_t>(deviceExtensions.size());
		deviceCreateInfo.ppEnabledExtensionNames = deviceExtensions.data();
		deviceCreateInfo.enabledLayerCount = 0;
		if (enableValidationLayer) {

			deviceCreateInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
			deviceCreateInfo.ppEnabledLayerNames = validationLayers.data();
		}

		if (vkCreateDevice(physicalDevice, &deviceCreateInfo, nullptr, &device)) {
			throw std::runtime_error("failed to create logical device!");
		}

		vkGetDeviceQueue(device, static_cast<uint32_t>(indices.graphicsFamily), 0, &graphicsQueue);
		vkGetDeviceQueue(device, static_cast<uint32_t>(indices.presentFamily), 0, &presentQueue);

	}


	VkSurfaceFormatKHR InitVulkan::chooseSwapChainSurfaceFormat() {
		std::vector<VkSurfaceFormatKHR> &format = swapChainDetail.formats;

		//la swap chain n'as pas format prefere, on prend ce que l'on veut
		if (format.size() == 1 && format[0].format == VK_FORMAT_UNDEFINED) {
			return {VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR};
		}

		for (const auto &formatAvailable : format) {
			if (formatAvailable.format == VK_FORMAT_B8G8R8A8_UNORM &&
			    formatAvailable.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
				return formatAvailable;
			}
		}
		return format[0];
	}

	VkPresentModeKHR InitVulkan::chooseSwapPresentMode() {

		VkPresentModeKHR bestMode = VK_PRESENT_MODE_FIFO_KHR;

		for (const auto &availablePresentMode : swapChainDetail.presentModes) {
			if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR) {
				return availablePresentMode;
			} else if (availablePresentMode == VK_PRESENT_MODE_IMMEDIATE_KHR) {
				bestMode = availablePresentMode;
			}
		}

		return bestMode;
	}

	VkExtent2D InitVulkan::chooseSwapExtent() {
		VkSurfaceCapabilitiesKHR &capabilities = swapChainDetail.capabilities;

		if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) {
			return capabilities.currentExtent;
		} else {
			VkExtent2D actualExtent{static_cast<uint32_t>(window.settings.width),
			                        static_cast<uint32_t>(window.settings.height)};

			actualExtent.width = std::max(capabilities.minImageExtent.width,
			                              std::max(capabilities.maxImageExtent.width, actualExtent.width));
			actualExtent.height = std::max(capabilities.minImageExtent.height,
			                               std::min(capabilities.maxImageExtent.height, actualExtent.height));

			return actualExtent;
		}
	}

	void InitVulkan::createImageView() {
		swapChainImageViews.resize(swapChainImage.size());
		for (uint32_t i = 0; i < swapChainImageViews.size(); i++) {
			VkImageViewCreateInfo createInfo{};
			createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
			createInfo.format = swapChainFormat;
			createInfo.image = swapChainImage[i];
			createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
			createInfo.components.a = VK_COMPONENT_SWIZZLE_A;
			createInfo.components.b = VK_COMPONENT_SWIZZLE_B;
			createInfo.components.r = VK_COMPONENT_SWIZZLE_R;
			createInfo.components.g = VK_COMPONENT_SWIZZLE_G;
			createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			createInfo.subresourceRange.baseMipLevel = 0;
			createInfo.subresourceRange.levelCount = 1;
			createInfo.subresourceRange.baseArrayLayer = 0;
			createInfo.subresourceRange.layerCount = 1;

			if (vkCreateImageView(device, &createInfo, nullptr, &swapChainImageViews[i])) {
				throw std::runtime_error("error to create image view");
			}
		}
	}

	void InitVulkan::createSwapChain() {
		VkSurfaceFormatKHR surfaceFormat = chooseSwapChainSurfaceFormat();
		VkPresentModeKHR presentMode = chooseSwapPresentMode();
		VkExtent2D extent = chooseSwapExtent();


		uint32_t imageCount = swapChainDetail.capabilities.minImageCount + 1;
		if (swapChainDetail.capabilities.maxImageCount > 0 && imageCount > swapChainDetail.capabilities.maxImageCount) {
			imageCount = swapChainDetail.capabilities.maxImageCount;
		}

		VkSwapchainCreateInfoKHR swapChainInfo{};
		swapChainInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
		swapChainInfo.surface = surface;
		swapChainInfo.minImageCount = imageCount;
		swapChainInfo.imageFormat = surfaceFormat.format;
		swapChainInfo.imageExtent = extent;
		swapChainInfo.imageColorSpace = surfaceFormat.colorSpace;
		swapChainInfo.imageArrayLayers = 1;
		swapChainInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

		// multiple queue
		uint32_t queueFamilyIndices[]{static_cast<uint32_t>(indices.graphicsFamily),
		                              static_cast<uint32_t>(indices.presentFamily)};

		if (indices.graphicsFamily != indices.presentFamily) {
			swapChainInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
			swapChainInfo.queueFamilyIndexCount = 2;
			swapChainInfo.pQueueFamilyIndices = queueFamilyIndices;
		} else {
			swapChainInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
			swapChainInfo.queueFamilyIndexCount = 0; // Optional
			swapChainInfo.pQueueFamilyIndices = nullptr; // Optional
		}

		swapChainInfo.preTransform = swapChainDetail.capabilities.currentTransform;
		swapChainInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
		swapChainInfo.presentMode = presentMode;
		swapChainInfo.clipped = VK_TRUE;
		swapChainInfo.oldSwapchain = VK_NULL_HANDLE;

		if (vkCreateSwapchainKHR(device, &swapChainInfo, nullptr, &swapChain)) {
			throw std::runtime_error("failed to create swapchain");
		}
		vkGetSwapchainImagesKHR(device, swapChain, &imageCount, nullptr);
		swapChainImage.resize(imageCount);
		vkGetSwapchainImagesKHR(device, swapChain, &imageCount, swapChainImage.data());

		swapChainExtent = extent;
		swapChainFormat = surfaceFormat.format;

		createImageView();
	}

	VkFormat InitVulkan::findSupportedFormat(std::vector<VkFormat> const &candidatees, VkImageTiling tiling,
	                                         VkFormatFeatureFlags features) {

		for (VkFormat format : candidatees) {
			VkFormatProperties props{};
			vkGetPhysicalDeviceFormatProperties(physicalDevice, format, &props);
			if (tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features) {
				return format;
			} else if (tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features) {
				return format;
			}
		}

		throw std::runtime_error("failed to find supported format!");
	}

	VkFormat InitVulkan::findDepthFormat() {
		return findSupportedFormat({VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT},
		                           VK_IMAGE_TILING_OPTIMAL, VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT);
	}

	void InitVulkan::createRenderPass() {
		VkAttachmentDescription colorAttachment{};
		colorAttachment.format = swapChainFormat;
		colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
		colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

		// depth attachement
		VkAttachmentDescription depthAttachment = {};
		depthAttachment.format = findDepthFormat();
		depthAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
		depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

		VkAttachmentReference depthAttachmentRef = {};
		depthAttachmentRef.attachment = 1;
		depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

		VkAttachmentReference colorAttachmentRef = {};
		colorAttachmentRef.attachment = 0;
		colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
//first subpass
		VkSubpassDescription subpass = {};
		subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
		subpass.colorAttachmentCount = 1;
		subpass.pColorAttachments = &colorAttachmentRef;
		subpass.pDepthStencilAttachment = &depthAttachmentRef;

		VkSubpassDependency dependency = {};
		dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
		dependency.dstSubpass = 0;
		dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dependency.srcAccessMask = 0;
		dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;


		std::array<VkSubpassDescription, 1> subpasses{subpass};
		std::array<VkSubpassDependency, 1> dependencies{dependency};
		std::array<VkAttachmentDescription, 2> attachments = {colorAttachment, depthAttachment};
		VkRenderPassCreateInfo renderPassInfo = {};
		renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
		renderPassInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
		renderPassInfo.pAttachments = attachments.data();
		renderPassInfo.subpassCount = subpasses.size();
		renderPassInfo.pSubpasses = subpasses.data();
		renderPassInfo.dependencyCount = dependencies.size();
		renderPassInfo.pDependencies = dependencies.data();


		if (vkCreateRenderPass(device, &renderPassInfo, nullptr, &renderpass) != VK_SUCCESS) {
			throw std::runtime_error("failed to create render pass!");
		}
	}

	void InitVulkan::createCommandPool() {

		VkCommandPoolCreateInfo poolInfo = {};
		poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
		poolInfo.queueFamilyIndex = static_cast<uint32_t>(indices.graphicsFamily);
		poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT; // Optional
		if (vkCreateCommandPool(device, &poolInfo, nullptr, &commandPool) != VK_SUCCESS) {
			throw std::runtime_error("failed to create command pool!");
		}
	}

	uint32_t InitVulkan::findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties) {
		VkPhysicalDeviceMemoryProperties memProperties{};
		vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memProperties);

		for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) {
			if ((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties) {
				return i;
			}
		}

		displayFatalError("failed to find suitable memory type!");

	}

	VkImageView
	InitVulkan::createImageView(VkImage image, VkFormat format, VkImageAspectFlags aspectFlags, uint32_t layerCount) {
		VkImageViewCreateInfo viewInfo = {};
		viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		viewInfo.image = image;
		viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		viewInfo.format = format;
		viewInfo.subresourceRange.aspectMask = aspectFlags;
		viewInfo.subresourceRange.baseMipLevel = 0;
		viewInfo.subresourceRange.levelCount = 1;
		viewInfo.subresourceRange.baseArrayLayer = 0;
		viewInfo.subresourceRange.layerCount = layerCount;

		VkImageView imageView;
		if (vkCreateImageView(device, &viewInfo, nullptr, &imageView) != VK_SUCCESS) {
			displayFatalError("failed to create texture image view!");
		}

		return imageView;
	}

	VkCommandBuffer InitVulkan::beginSingleTimeCommands() {
		VkCommandBufferAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		allocInfo.commandPool = commandPool;
		allocInfo.commandBufferCount = 1;

		VkCommandBuffer commandBuffer;
		vkAllocateCommandBuffers(device, &allocInfo, &commandBuffer);

		VkCommandBufferBeginInfo beginInfo = {};
		beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

		vkBeginCommandBuffer(commandBuffer, &beginInfo);

		return commandBuffer;
	}

	bool hasStencilComponent(VkFormat format) {
		return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
	}

	void InitVulkan::endSingleTimeCommands(VkCommandBuffer commandBuffer) {
		vkEndCommandBuffer(commandBuffer);

		VkSubmitInfo submitInfo = {};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submitInfo.commandBufferCount = 1;
		submitInfo.pCommandBuffers = &commandBuffer;

		vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);
		vkQueueWaitIdle(graphicsQueue);

		vkFreeCommandBuffers(device, commandPool, 1, &commandBuffer);
	}

	void InitVulkan::transitionImageLayout(VkImage image, VkFormat format, VkImageLayout oldLayout,
	                                       VkImageLayout newLayout,
	                                       int layerCount) {
		VkCommandBuffer commandBuffer = beginSingleTimeCommands();

		VkImageMemoryBarrier barrier = {};
		barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		barrier.oldLayout = oldLayout;
		barrier.newLayout = newLayout;
		barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		barrier.image = image;

		if (newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
			barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;

			if (hasStencilComponent(format)) {
				barrier.subresourceRange.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
			}
		} else {
			barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		}

		barrier.subresourceRange.baseMipLevel = 0;
		barrier.subresourceRange.levelCount = 1;
		barrier.subresourceRange.baseArrayLayer = 0;
		barrier.subresourceRange.layerCount = static_cast<uint32_t>(layerCount);

		VkPipelineStageFlags sourceStage;
		VkPipelineStageFlags destinationStage;

		if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
			barrier.srcAccessMask = 0;
			barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

			sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
			destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
		} else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL &&
		           newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
			barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

			sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
			destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
		} else if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED &&
		           newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
			barrier.srcAccessMask = 0;
			barrier.dstAccessMask =
					VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

			sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
			destinationStage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
		} else {
			throw std::invalid_argument("unsupported layout transition!");
		}

		vkCmdPipelineBarrier(
				commandBuffer,
				sourceStage, destinationStage,
				0,
				0, nullptr,
				0, nullptr,
				1, &barrier
		);

		endSingleTimeCommands(commandBuffer);
	}

	void InitVulkan::createDepthImage() {
		VkFormat depthFormat = findDepthFormat();

		createImage(swapChainExtent.width, swapChainExtent.height, depthFormat, VK_IMAGE_TILING_OPTIMAL,
		            VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, depthImage,
		            depthImageMemory);

		depthImageView = createImageView(depthImage, depthFormat, VK_IMAGE_ASPECT_DEPTH_BIT, 1);

		transitionImageLayout(depthImage, depthFormat, VK_IMAGE_LAYOUT_UNDEFINED,
		                      VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
		                      1);
	}

	void InitVulkan::createImage(uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling,
	                             VkImageUsageFlags usage, VkMemoryPropertyFlags properties, VkImage &image,
	                             VkDeviceMemory &imageMemory) {
		VkImageCreateInfo imageInfo = {};
		imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		imageInfo.imageType = VK_IMAGE_TYPE_2D;
		imageInfo.extent.width = width;
		imageInfo.extent.height = height;
		imageInfo.extent.depth = 1;
		imageInfo.mipLevels = 1;
		imageInfo.arrayLayers = 1;
		imageInfo.format = format;
		imageInfo.tiling = tiling;
		imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		imageInfo.usage = usage;
		imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
		imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

		if (vkCreateImage(device, &imageInfo, nullptr, &image) != VK_SUCCESS) {
			throw std::runtime_error("failed to create image!");
		}

		VkMemoryRequirements memRequirements;
		vkGetImageMemoryRequirements(device, image, &memRequirements);

		VkMemoryAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		allocInfo.allocationSize = memRequirements.size;
		allocInfo.memoryTypeIndex = findMemoryType(memRequirements.memoryTypeBits, properties);

		if (vkAllocateMemory(device, &allocInfo, nullptr, &imageMemory) != VK_SUCCESS) {
			throw std::runtime_error("failed to allocate image memory!");
		}

		vkBindImageMemory(device, image, imageMemory, 0);
	}

	void InitVulkan::createFrameBuffers() {
		framebuffers.resize(swapChainImageViews.size());

		for (unsigned i = 0; i < framebuffers.size(); i++) {
			VkImageView attachment[] = {
					swapChainImageViews[i],
					depthImageView
			};
			VkFramebufferCreateInfo framebufferInfo{};

			framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
			framebufferInfo.renderPass = renderpass;
			framebufferInfo.attachmentCount = 2;
			framebufferInfo.pAttachments = attachment;
			framebufferInfo.width = swapChainExtent.width;
			framebufferInfo.height = swapChainExtent.height;
			framebufferInfo.layers = 1;

			auto res = vkCreateFramebuffer(device, &framebufferInfo, nullptr, &framebuffers[i]);
			checkFatalResult(res, "failed to create framebuffer!");


		}
	}

	void InitVulkan::createPipelineCache() {
		VkPipelineCacheCreateInfo createInfo{};
		createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;
		auto res = vkCreatePipelineCache(device, &createInfo, nullptr, &pipelineCache);
		checkFatalResult(res, "unable to initialize pipelineCache");

	}

	bufferData InitVulkan::createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties) {

		VkBuffer buffer;
		VkDeviceMemory bufferMemory;
		VkBufferCreateInfo bufferInfo = {};
		bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		bufferInfo.size = size;
		bufferInfo.usage = usage;
		bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

		if (vkCreateBuffer(device, &bufferInfo, nullptr, &buffer) != VK_SUCCESS) {
			throw std::runtime_error("failed to create buffer!");
		}

		VkMemoryRequirements memRequirements;
		vkGetBufferMemoryRequirements(device, buffer, &memRequirements);

		VkMemoryAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		allocInfo.allocationSize = memRequirements.size;
		allocInfo.memoryTypeIndex = findMemoryType(memRequirements.memoryTypeBits, properties);

		if (vkAllocateMemory(device, &allocInfo, nullptr, &bufferMemory) != VK_SUCCESS) {
			throw std::runtime_error("failed to allocate buffer memory!");
		}

		vkBindBufferMemory(device, buffer, bufferMemory, 0);

		return {buffer, bufferMemory, device};
	}

	void InitVulkan::createUniformBuffer() {
		VkDeviceSize size = sizeof(uboCam);

		camBuffer = Buffer(createBuffer(size, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
		                                VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT));


	}

	void InitVulkan::createDescriptorSetLayout() {

		VkDescriptorSetLayoutBinding bindingLayout{};
		bindingLayout.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
		bindingLayout.binding = 0;
		bindingLayout.descriptorCount = 1;
		bindingLayout.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;

		VkDescriptorSetLayoutBinding bindSampler{};
		bindSampler.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
		bindSampler.binding = 1;
		bindSampler.descriptorCount = numberTextureAllowed;
		bindSampler.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;

		VkDescriptorSetLayoutBinding bindDynamicUni{};
		bindDynamicUni.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
		bindDynamicUni.binding = 0;
		bindDynamicUni.descriptorCount = 1;
		bindDynamicUni.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;


		std::vector<VkDescriptorSetLayoutBinding> tab{bindingLayout, bindSampler};

		VkDescriptorSetLayoutCreateInfo createInfo{};
		createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		createInfo.bindingCount = tab.size();
		createInfo.pBindings = tab.data();

		auto res = vkCreateDescriptorSetLayout(device, &createInfo, nullptr, &uboDescriptor);
		checkFatalResult(res, "unable to create desciptorSetLayout");
		createInfo.bindingCount = 1;
		createInfo.pBindings = &bindDynamicUni;
		res = vkCreateDescriptorSetLayout(device, &createInfo, nullptr, &uboDescriptorDynamic);
		checkFatalResult(res, "unable to create desciptorSetLayout");

	}


	void InitVulkan::createDescriptorPool() {

		VkDescriptorPoolSize poolUniBuffer{};
		poolUniBuffer.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		poolUniBuffer.descriptorCount = 1;

		VkDescriptorPoolSize poolDynamicUniBuffer{};
		poolDynamicUniBuffer.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
		poolDynamicUniBuffer.descriptorCount = 1;

		VkDescriptorPoolSize poolImageSamplerBuffer{};
		poolImageSamplerBuffer.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		poolImageSamplerBuffer.descriptorCount = numberTextureAllowed;

		std::vector<VkDescriptorPoolSize> tab{poolUniBuffer, poolDynamicUniBuffer, poolImageSamplerBuffer};
		uint32_t size = 0;
		for(auto & desc : tab){
			size += desc.descriptorCount;
		}

		VkDescriptorPoolCreateInfo createInfo{};
		createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
		createInfo.maxSets = size;
		createInfo.poolSizeCount = static_cast<uint32_t>(tab.size());
		createInfo.pPoolSizes = tab.data();

		auto res = vkCreateDescriptorPool(device, &createInfo, nullptr, &descriptorPool);
		checkFatalResult(res, " unable to create descriptor pool");
	}

	void InitVulkan::createDescriptorSet() {

		VkDescriptorSetAllocateInfo allocateInfo{};
		allocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		allocateInfo.descriptorPool = descriptorPool;
		allocateInfo.descriptorSetCount = 1;
		allocateInfo.pSetLayouts = &uboDescriptor;

		auto res = vkAllocateDescriptorSets(device, &allocateInfo, &descriptorSet);
		checkFatalResult(res, "unable to allocate descriptor set");
		allocateInfo.pSetLayouts = &uboDescriptorDynamic;
		res = vkAllocateDescriptorSets(device, &allocateInfo, &descriptorSetDynamic);
		checkFatalResult(res, "unable to allocate descriptor set");


		bufferInfo.offset = 0;
		bufferInfo.range = sizeof(uboCam);
		bufferInfo.buffer = camBuffer;


		VkWriteDescriptorSet & writeDescriptorSet = writesDescriptors[0];
		writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		writeDescriptorSet.descriptorCount = 1;
		writeDescriptorSet.dstSet = descriptorSet;
		writeDescriptorSet.dstBinding = 0;
		writeDescriptorSet.dstArrayElement = 0;
		writeDescriptorSet.pBufferInfo = &bufferInfo;

		VkWriteDescriptorSet & imageDescriptorSet = writesDescriptors[1];
		imageDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		imageDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		imageDescriptorSet.descriptorCount = 1;
		imageDescriptorSet.dstSet = descriptorSet;
		imageDescriptorSet.dstBinding = 1;
		imageDescriptorSet.dstArrayElement = 0;
		imageDescriptorSet.pImageInfo = imageInfos.data();

		vkUpdateDescriptorSets(device, 1, &writeDescriptorSet, 0, nullptr);

	}


	void InitVulkan::defineVertexInputSetting() {
		vertexInputDescription.binding = 0;
		vertexInputDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
		vertexInputDescription.stride = sizeof(vertices);

		vertexAttributeDescription[0].binding = 0;
		vertexAttributeDescription[0].location = 0;
		vertexAttributeDescription[0].format = VK_FORMAT_R32G32B32_SFLOAT;
		vertexAttributeDescription[0].offset = static_cast<uint32_t>(offsetof(vertices, pos));

		vertexAttributeDescription[1].binding = 0;
		vertexAttributeDescription[1].location = 1;
		vertexAttributeDescription[1].format = VK_FORMAT_R32G32B32_SFLOAT;
		vertexAttributeDescription[1].offset = static_cast<uint32_t>(offsetof(vertices, uv));

		vertexAttributeDescription[2].binding = 0;
		vertexAttributeDescription[2].location = 2;
		vertexAttributeDescription[2].format = VK_FORMAT_R32G32B32_SFLOAT;
		vertexAttributeDescription[2].offset = static_cast<uint32_t>(offsetof(vertices, normals));
	}

	void InitVulkan::createPipelineLayout() {

		VkPushConstantRange pushConstantRange{};
		pushConstantRange.offset = 0;
		pushConstantRange.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
		pushConstantRange.size = sizeof(int);

		std::vector<VkDescriptorSetLayout> layouts{uboDescriptor, uboDescriptorDynamic};

		VkPipelineLayoutCreateInfo createInfo{};
		createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		createInfo.setLayoutCount = static_cast<uint32_t>(layouts.size());
		createInfo.pSetLayouts = layouts.data();
		createInfo.pushConstantRangeCount = 1;
		createInfo.pPushConstantRanges = &pushConstantRange;

		pipelinesLayout.emplace(std::pair{"basic", VkPipelineLayout{}});

		auto res = vkCreatePipelineLayout(device, &createInfo, nullptr, &pipelinesLayout["basic"]);
		checkFatalResult(res, "unable to create piplineLayout");


	}

	VkShaderModule InitVulkan::createShaderModule(std::vector<char> &&code) {
		VkShaderModuleCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		createInfo.codeSize = code.size();
		createInfo.pCode = reinterpret_cast<const uint32_t *>(code.data());
		VkShaderModule shaderModule;

		auto res = vkCreateShaderModule(device, &createInfo, nullptr, &shaderModule);
		checkFatalResult(res, "failed to create shader module!");

		return shaderModule;
	}

	VkPipelineShaderStageCreateInfo InitVulkan::createShaderInfo(shaderInfo &&shader) {
		shader.data = readFile(shader.pathShader);

		VkPipelineShaderStageCreateInfo createInfo{};
		createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		createInfo.stage = shader.stage;
		createInfo.module = createShaderModule(std::move(shader.data));
		createInfo.pName = "main";

		return createInfo;
	}

	InitVulkan &InitVulkan::getInstance() {
		return *initVulkan;
	}

	//TODO: change that because map is very weigth, so find a way to unmap the memory only when you destroy it
	void InitVulkan::copyDataToBuffer(const void *data, unsigned size, VkDeviceMemory memory) {
		void *copyData;
		vkMapMemory(device, memory, 0, size, 0, &copyData);
		memcpy(copyData, data, size);
		vkUnmapMemory(device, memory);

	}


	void InitVulkan::copyDataToBufferImage(VkBuffer buffer, VkImage image, int width, int height, int layer) {
		VkCommandBuffer commandBuffer = beginSingleTimeCommands();

		VkBufferImageCopy region = {};
		region.bufferOffset = 0;
		region.bufferRowLength = 0;
		region.bufferImageHeight = 0;

		region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		region.imageSubresource.mipLevel = 0;
		region.imageSubresource.baseArrayLayer = static_cast<uint32_t>(layer);
		region.imageSubresource.layerCount = 1;

		region.imageOffset = {0, 0, 0};
		region.imageExtent = {
				static_cast<uint32_t>(width),
				static_cast<uint32_t>(height),
				1
		};
		vkCmdCopyBufferToImage(
				commandBuffer,
				buffer,
				image,
				VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
				1,
				&region
		);
		endSingleTimeCommands(commandBuffer);
	}

	void InitVulkan::copyBuffer(VkBuffer src, VkBuffer dest, VkDeviceSize size) {
		VkCommandBuffer commandBuffer = beginSingleTimeCommands();

		VkBufferCopy copy{};
		copy.size = size;
		copy.dstOffset = 0;
		copy.srcOffset = 0;

		vkCmdCopyBuffer(commandBuffer, src, dest, 1, &copy);
		endSingleTimeCommands(commandBuffer);

	}

	Buffer InitVulkan::createVertexBuffer(std::string &&path) {
		vkEngine::parsing_data data = vkEngine::parseObj(std::move(path));
		std::vector<vertices> models;
		models.reserve(data.vertices.size() / 3);


		bufferData bufferData1{};
		for (unsigned i = 0; i < data.vertices.size(); i++) {
			models.emplace_back();
			models[i].pos = glm::normalize(data.vertices[i]);
			models[i].uv = data.uvs[i];
			models[i].normals = data.normals[i];
		}

		auto size = static_cast<unsigned int>(models.size() * sizeof(vertices));
		auto [stagingBuffer, stagingMemory, tmp] = createBuffer(size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
		             VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);

		copyDataToBuffer(models.data(), size, stagingMemory);
		bufferData1 = createBuffer(size, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
		             VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
		copyBuffer(stagingBuffer, bufferData1.buffer, size);

		vkDestroyBuffer(device, stagingBuffer, nullptr);
		vkFreeMemory(device, stagingMemory, nullptr);

		bufferData1.device  = device;
		return Buffer(bufferData1);
	}

	void InitVulkan::createDynamicBuffer() {


		VkPhysicalDeviceProperties pro{};
		vkGetPhysicalDeviceProperties(physicalDevice, &pro);

		minUboAlignment = pro.limits.minUniformBufferOffsetAlignment;

		size_t sizeBuffer = vkEngine::Node::getRoot()->size() * minUboAlignment ;
		if(sizeBuffer == 0){
			return;
		}

		if(dynamicUboModel.model){
			free(dynamicUboModel.model);
		}
		dynamicUboModel.model = (glm::mat4*)aligned_alloc(minUboAlignment, sizeBuffer);
		dynamicModelBuffer = Buffer(createBuffer(sizeBuffer, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
		                                         VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT ));

		VkDescriptorBufferInfo dynamicBuffer{};
		dynamicBuffer.offset = 0;
		dynamicBuffer.range = minUboAlignment;
		dynamicBuffer.buffer = dynamicModelBuffer;

		VkWriteDescriptorSet writeDescriptorSet{};
		writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
		writeDescriptorSet.descriptorCount = 1;
		writeDescriptorSet.dstSet = descriptorSetDynamic;
		writeDescriptorSet.dstBinding = 0;
		writeDescriptorSet.dstArrayElement = 0;
		writeDescriptorSet.pBufferInfo = &dynamicBuffer;


		vkUpdateDescriptorSets(device, 1, &writeDescriptorSet, 0, nullptr);


	}

	textureData InitVulkan::createTexture(std::string const &path) {

		textureData data{};
		data.device = device;
		int textureWidth, textureHeight, textureChannel;
		stbi_uc * pixels = stbi_load(path.c_str(), &textureWidth, &textureHeight, &textureChannel, STBI_rgb_alpha);

		if(!pixels){
			std::cerr << "failed to load" << path <<std::endl;
			throw;
		}
		VkDeviceSize size = static_cast<VkDeviceSize>(textureHeight * textureWidth * 4);
		Buffer bufferStaging{createBuffer(size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
		                                  VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT)};
		copyDataToBuffer(pixels, size, bufferStaging.getMemory());
		createImage(textureWidth, textureHeight,
		            VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_TILING_OPTIMAL,
		            VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
		            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
		            data.image, data.memory);

		transitionImageLayout(data.image, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_UNDEFINED,
		                      VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
		                      1);

		copyDataToBufferImage((VkBuffer) bufferStaging, data.image, textureWidth, textureHeight, 0);

		data.view = createImageView(data.image, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_ASPECT_COLOR_BIT, 1);
		VkSamplerCreateInfo samplerInfo = {};
		samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
		samplerInfo.magFilter = VK_FILTER_LINEAR;
		samplerInfo.minFilter = VK_FILTER_LINEAR;
		samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
		samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
		samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
		samplerInfo.anisotropyEnable = VK_TRUE;
		samplerInfo.maxAnisotropy = 16;
		samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
		samplerInfo.unnormalizedCoordinates = VK_FALSE;
		samplerInfo.compareEnable = VK_FALSE;
		samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;

		samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
		samplerInfo.mipLodBias = 0.0f;
		samplerInfo.minLod = 0.0f;
		samplerInfo.maxLod = 0.0f;

		if (vkCreateSampler(device, &samplerInfo, nullptr, &data.sampler) != VK_SUCCESS) {
			throw std::runtime_error("failed to create texture sampler!");
		}
		return data;
	}



	void InitVulkan::createCommandBuffer() {

		commandBuffers.resize(swapChainImageViews.size());

		VkCommandBufferAllocateInfo allocateInfo{};
		allocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		allocateInfo.commandPool = commandPool;
		allocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		allocateInfo.commandBufferCount = static_cast<uint32_t>(commandBuffers.size());


		auto res = vkAllocateCommandBuffers(device, &allocateInfo, commandBuffers.data());
		checkFatalResult(res, "failed to allocate command buffers!");


	}

	void InitVulkan::updateUni() {
		auto root = vkEngine::Node::getRoot();
		uboCam tmp{root->getView(), root->getProj()};
		copyDataToBuffer(&tmp, sizeof(tmp), camBuffer.getMemory());
		vkEngine::forEachNode([modelMemory = dynamicUboModel.model, align = minUboAlignment](vkEngine::Node const* node){

			auto modelMat = (glm::mat4*)(((uint64_t)modelMemory + (node->getId()*align)));
			*modelMat = node->getTransform();
		});

		copyDataToBuffer(dynamicUboModel.model, vkEngine::Node::getRoot()->size()*minUboAlignment, dynamicModelBuffer.getMemory());


	}

	void InitVulkan::updateDescriptorTexture() {

		auto &texture = vkEngine::TextureManager::last();
		VkDescriptorImageInfo imageInfo{};
		imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		imageInfo.sampler = texture.getSampler();
		imageInfo.imageView = texture.getImageView();

		imageInfos.push_back(imageInfo);

		writesDescriptors[1].descriptorCount = imageInfos.size();
		writesDescriptors[1].pImageInfo = imageInfos.data();


		vkUpdateDescriptorSets(device, writesDescriptors.size(), writesDescriptors.data(), 0, nullptr);



	}

	void recordNode(const vkEngine::Node *node, int &id, VkCommandBuffer &commandBuffer,
	                VkPipelineLayout &pipelineLayout,
	                size_t alignment, VkDescriptorSet const *descriptor) {

		vkEngine::model3d const &model = node->getModel();
		vkEngine::mesh const &meshs = vkEngine::MeshManager::get(model.meshId);

		VkDeviceSize offset = 0;
		if(id != model.meshId) {
			vkCmdBindVertexBuffers(commandBuffer, 0, 1, meshs.verticesModel.getPtrBuffer(), &offset);
			id = model.meshId;
		}
		auto offsetDes = static_cast<uint32_t>(node->getId() * alignment);
		vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 1, 1, descriptor, 1, &offsetDes);

		int i = 0;
		if(model.textures.size() != 1){
			i = 1;
		}
		for (auto &range : meshs.range) {
			if(model.textures[i] != -1){
				int a = model.textures[i++];
				vkCmdPushConstants(commandBuffer, pipelineLayout, VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(int), &a);
			}
			vkCmdDraw(commandBuffer, static_cast<uint32_t>(range.second), 1, static_cast<uint32_t>(range.first), 0);

		}
	}

	void InitVulkan::recordCommandBuffer() {
		vkEngine::Node::getRoot()->assignNumber();
		createDynamicBuffer();
		for (unsigned i = 0; i < commandBuffers.size(); i++) {
			lastMeshId = -1;
			vkResetCommandBuffer(commandBuffers[i], VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCES_BIT);
			VkCommandBufferBeginInfo begin{};
			begin.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
			begin.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;

			vkBeginCommandBuffer(commandBuffers[i], &begin);

			VkRenderPassBeginInfo passBeginInfo{};
			passBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
			passBeginInfo.renderPass = renderpass;
			passBeginInfo.framebuffer = framebuffers[i];
			passBeginInfo.renderArea.offset = {0, 0};
			passBeginInfo.renderArea.extent = swapChainExtent;
			std::array<VkClearValue, 2> clearValues = {};
			clearValues[0].color = {0.5f, 0.5f, 0.5f, 1.0f};
			clearValues[1].depthStencil = {1.0f, 0};

			passBeginInfo.clearValueCount = 2;
			passBeginInfo.pClearValues = clearValues.data();



			vkCmdBeginRenderPass(commandBuffers[i], &passBeginInfo, VK_SUBPASS_CONTENTS_INLINE);
			vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pipelines["basic"]);
			vkCmdBindDescriptorSets(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pipelinesLayout["basic"], 0, 1,
			                        &descriptorSet, 0, nullptr);


			vkEngine::forEachNode(
					[&id = lastMeshId, &commandBuffer = commandBuffers[i], &pipelineLayout = pipelinesLayout["basic"],
							alignment = minUboAlignment, set = &descriptorSetDynamic]
							(vkEngine::Node const * node){
						recordNode(node, id, commandBuffer, pipelineLayout, alignment, set);
			});


			vkCmdEndRenderPass(commandBuffers[i]);
			vkEndCommandBuffer(commandBuffers[i]);
		}

	}

	void InitVulkan::createSemaphore() {
		VkSemaphoreCreateInfo createInfo{};
		createInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
		createInfo.pNext = nullptr;
		createInfo.flags = 0;
		auto res = vkCreateSemaphore(device, &createInfo, nullptr, &imageAvailableSemaphore)
		           || vkCreateSemaphore(device, &createInfo, nullptr, &renderFinishedSemaphore);

		checkFatalResult((VkResult) res, "unable to create Semaphores");

	}



	void InitVulkan::needNewRecord() {
		needRecord = true;

	}
	void InitVulkan::drawPipeline() {
		uint32_t imageIndex;

		vkAcquireNextImageKHR(device, (VkSwapchainKHR) swapChain, std::numeric_limits<uint64_t>::max(),
		                      imageAvailableSemaphore, VK_NULL_HANDLE, &imageIndex);

		VkSubmitInfo submitInfo{};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submitInfo.commandBufferCount = 1;
		VkCommandBuffer a = commandBuffers[imageIndex];
		submitInfo.pCommandBuffers = &a;

		VkPipelineStageFlags waitStage[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
		submitInfo.waitSemaphoreCount = 1;
		submitInfo.pWaitSemaphores = &imageAvailableSemaphore;
		submitInfo.pWaitDstStageMask = waitStage;
		submitInfo.signalSemaphoreCount = 1;
		submitInfo.pSignalSemaphores = &renderFinishedSemaphore;

		if (vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE) != VK_SUCCESS) {
			displayError("failed to submit draw command planetBuffer!");
		}


		VkSwapchainKHR swapChains[] = {swapChain};
		VkPresentInfoKHR presentInfo{};
		presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
		presentInfo.waitSemaphoreCount = 1;
		presentInfo.pWaitSemaphores = &renderFinishedSemaphore;
		presentInfo.swapchainCount = 1;
		presentInfo.pImageIndices = &imageIndex;
		presentInfo.pSwapchains = swapChains;
		presentInfo.pNext = nullptr;

		vkQueuePresentKHR(presentQueue, &presentInfo);
	}

	void InitVulkan::draw() {
		if(needRecord){
			recordCommandBuffer();
			needRecord = false;
		}
		// TODO need to do that only if something change
		updateUni();
		drawPipeline();



	}

	void InitVulkan::init(const Window &window, const VkPhysicalDeviceFeatures &desiredFeature, int numberTexture) {
		//TODO : change that
		initVulkan = makeUnique(window, desiredFeature, numberTexture);
		initVulkan->isInit = true;
	}

	bool InitVulkan::shouldWindowClose() {
		return glfwWindowShouldClose(window);
	}

	InitVulkan::InitVulkan(const Window &window, const VkPhysicalDeviceFeatures &desiredFeature,
		                       unsigned numberTexture) : numberTextureAllowed(numberTexture), window(window),
	                                                 desiredFeature(desiredFeature)
														{

		createInstance();
		createSurface();
		choosePhysicalDevice();
		createLogicalDevice();
		createSwapChain();
		createRenderPass();
		createCommandPool();
		createDepthImage();
		createFrameBuffers();
		createPipelineCache();
		createUniformBuffer();
		createDescriptorSetLayout();
		createDescriptorPool();
		createDescriptorSet();
		defineVertexInputSetting();
		createPipelineLayout();
		createCommandBuffer();
		createSemaphore();

		vkEngine::Node::getRoot()->changeCam(
				new vkEngine::PerspectiveCamera{{5, 0, -15},
				                     {0, 0, 0},
				                     window.settings.width/ static_cast<float>(window.settings.height)});

	}

	void InitVulkan::destroyDescriptor() {
		vkDestroyDescriptorSetLayout(device, uboDescriptorDynamic, nullptr);

		vkDestroyDescriptorSetLayout(device, uboDescriptor, nullptr);

		vkDestroyDescriptorPool(device, descriptorPool, nullptr);
	}

	void InitVulkan::destroyUniformBuffer() {
	//	vkDestroyBuffer(device, uboBuffer, nullptr);
	//	vkFreeMemory(device, uboMemory, nullptr);
		camBuffer.destroy();
		dynamicModelBuffer.destroy();
	}

	void InitVulkan::destroyPipeline() {


		for (auto & [key, value] : pipelines) {
			vkDestroyPipeline(device, value, nullptr);
		}
		for (auto & [key, value] : pipelinesLayout) {
			vkDestroyPipelineLayout(device, value, nullptr);
		}
		vkDestroyPipelineCache(device, pipelineCache, nullptr);

	}

	InitVulkan::~InitVulkan() {
//		terminate();
	}

	void InitVulkan::terminate() {
		if (isInit) {
			free(dynamicUboModel.model);
			vkDeviceWaitIdle(device);
			vkEngine::MeshManager::manager.models.empty();
			vkEngine::TextureManager::textureManager.textures.empty();
			vkDestroySemaphore(device, imageAvailableSemaphore, nullptr);
			vkDestroySemaphore(device, renderFinishedSemaphore, nullptr);
			destroyPipeline();
			destroyDescriptor();
			destroyUniformBuffer();

			for (auto &framebuffer: framebuffers) {
				vkDestroyFramebuffer(device, framebuffer, nullptr);
			}
			vkDestroyImageView(device, depthImageView, nullptr);
			vkDestroyImage(device, depthImage, nullptr);
			vkFreeMemory(device, depthImageMemory, nullptr);
			vkDestroyCommandPool(device, commandPool, nullptr);
			vkDestroyRenderPass(device, renderpass, nullptr);
			vkDestroySwapchainKHR(device, swapChain, nullptr);
			for (auto &imageView: swapChainImageViews) {
				vkDestroyImageView(device, imageView, nullptr);
			}
			vkDestroyDevice(device, nullptr);
			vkDestroySurfaceKHR(instance, surface, nullptr);
			vkDestroyInstance(instance, nullptr);
		}
	}

}







































