//
// Created by stiven on 18-01-08.
//

#include "vkEngine/vulkan/textureSampler.hpp"
namespace vkEngine{

	void TextureSampler::emptyingTexture(TextureSampler &&old) {
		image = old.image;
		view = old. view;
		sampler = old.sampler;
		memory = old.memory;
		device = old.device;

		old.image = nullptr;
		old. view = nullptr;
		old.sampler = nullptr;
		old.memory = nullptr;
		old.device = nullptr;
	}

	TextureSampler::TextureSampler(textureData &&data):
	                                                   memory(data.memory),
	                                                   sampler(data.sampler),
	                                                   image(data.image),
	                                                   view(data.view ),
														device(data.device){

	}

	TextureSampler::TextureSampler(TextureSampler &&old) {
			emptyingTexture(std::move(old));
	}

	TextureSampler &TextureSampler::operator=(TextureSampler &&old) {
		emptyingTexture(std::move(old));
		return *this;
	}

	void TextureSampler::destroy() {
		if(image){
			vkDestroyImage(device, image, nullptr);
			vkDestroyImageView(device, view, nullptr);
			vkDestroySampler(device, sampler, nullptr);
			vkFreeMemory(device, memory, nullptr);
		}

	}

	TextureSampler::~TextureSampler() {
		destroy();
	}

	VkSampler TextureSampler::getSampler() const {
		return sampler;
	}

	VkImage TextureSampler::getImage() const {
		return image;
	}

	VkImageView TextureSampler::getImageView() const {
		return view;
	}
}