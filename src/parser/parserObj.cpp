//
// Created by stiven on 17-09-23.
//
#include <vkEngine/parser/parser.h>
#include <fstream>
#include <sstream>
#include <array>
#include <iostream>
#include <map>
#include <utils/stringUtil.h>
#include <vkEngine/utils/utils.h>
#include <utils/stlUtils.h>
namespace vkEngine{
    using namespace utils;
    static int constexpr bufferSize = 500;
void split(std::string & string, std::array<int, 3>& order);

void parseMtl(std::string const &path, mapMaterials &materials) {
    std::ifstream file{path};
    material *currentMaterial = nullptr;
    if(!file.is_open()){
        throw std::runtime_error("file " + path+"not found");
    }
    std::string token;
    std::string pathToFile = utils::extractPath(path);
    float x, y, z;
    while(file >> token){
        if(token == "newmtl"){
            file >> token;
            materials.emplace(std::make_pair(token, material()));
            currentMaterial = &materials[token];
        }else if(token == "map_Kd"){
            file >> currentMaterial->diffuseTexturePath ;
            currentMaterial->diffuseTexturePath = pathToFile+*currentMaterial->diffuseTexturePath;

        }else if(token == "Ka"){
            file >> x >> y >> z;
            currentMaterial->ambient = glm::vec3{x, y, z};
        }else if(token == "Kd"){
            file >> x >> y >> z;
            currentMaterial->diffuse = glm::vec3{x, y, z};
        }else if(token == "Ks"){
            file >> x >> y >> z;
            currentMaterial->specular = glm::vec3{x, y, z};
        }else if (token == "Ns"){
            file >> currentMaterial->exposant;
        }else if (token == "illum"){
            file >> currentMaterial->shininess;
        }else if(token == "map_Ka"){
            file >> currentMaterial->ambientTexturePath ;
            currentMaterial->ambientTexturePath = pathToFile+*currentMaterial->ambientTexturePath;
        }else if(token == "map_Ks"){
            file >> currentMaterial->specularTexturePath;
            currentMaterial->specularTexturePath = pathToFile+*currentMaterial->specularTexturePath;

        }
    }

}
void split(std::string & string, std::array<int, 3>& order){
    std::stringstream stream {string};
    char number[50]={0};
    for(unsigned i = 0; i <order.size() ; i++ ){
        stream.getline(number, 50, '/');
        order[i] = std::atoi(number)-1;
    }
}

void parseV(std::stringstream & stream, parsing_data& data){
    float x, y, z;
    stream  >> x >> y >> z;
    data.vertices.emplace_back(glm::vec3{x, y, z});


}
void parseVt(std::stringstream & stream, parsing_data& data){
    float x, y;
    stream >> x >> y ;
    data.uvs.emplace_back(glm::vec2{x, y});

}
void parseVn(std::stringstream & stream, parsing_data& data){
    float x, y, z;
    stream >> x >> y >> z;
    data.normals.emplace_back(glm::vec3{x, y, z});

}
void parseF(std::stringstream & stream, parsing_data& data, parsing_data & realData){

    std::array<std::string, 3> parseData;
    stream >> parseData[0] >> parseData[1]>> parseData[2];
    std::array<int, 3> order{};

    for(unsigned i = 0 ; i < parseData.size() ; i++){
        split(parseData[i], order) ;
        realData.vertices.push_back(data.vertices[order[0]]);
        realData.uvs.push_back(data.uvs[order[1]]);
        realData.normals.push_back(data.normals[order[2]]);
    }


}

void parseOneObj(std::ifstream &stream, parsing_data &data, std::string const &path) {
    static int constexpr size = 2;
    std::stringstream strStream;
    char line[bufferSize]={0};
    char type[size];
    parsing_data datas;
    int currentFace = 0;
    int last = 0;
    std::string lastMtl;
    std::string test;
    bool mtl ;
    while(true){
        test.clear();
        mtl = false;
        stream.getline(line, bufferSize, '\n');
        if(stream.eof()){
            if(!lastMtl.empty()){
                data.materials[lastMtl].count = currentFace - *data.materials[lastMtl].first;
            }
            break;
        }
        strStream = std::stringstream(line);
        strStream >> test;
        switch(test[0]){
            case 'v':
                switch(test[1]){

                    case 'n':
                        parseVn(strStream,datas);
                        break;
                    case 't':
                        parseVt(strStream,datas);
                        break;

                    default:
                        parseV(strStream,datas);
                        break;
                }
                break;
            case 'f':
                parseF(strStream, datas, data);
                mtl = true;
                break;
            case 'o':
                //parseOneObj(stream, data);
                break;
            case 'm':
                std::string testc;
                strStream >> testc;
                strStream >> testc;
                parseMtl(path + testc, data.materials);
                break;

        }
        if(test == "usemtl"){
            strStream >> test;
            data.materials[test].first = currentFace;
            if(!lastMtl.empty()){
                data.materials[lastMtl].count = currentFace - *data.materials[lastMtl].first ;
            }
            lastMtl = test;

        }
        if(mtl){
            currentFace += 3;
        }

    }


}

parsing_data const parseObj(std::string const & pathFile){

    parsing_data  data;
    std::ifstream file{pathFile};
    if(!file.is_open()){
        throw std::runtime_error("file " + pathFile +"not found");
    }
    parseOneObj(file, data, utils::extractPath(pathFile));

    return data;
}

}