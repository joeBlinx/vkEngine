//
// Created by stiven on 17-10-14.
//
#include <vkEngine/utils/axis.h>
#include <glm/glm.hpp>
namespace vkEngine {
    axis const worldAxis{
            glm::vec3{1, 0, 0},
            glm::vec3{0, 1, 0},
            glm::vec3{0, 0, 1}
    };
}