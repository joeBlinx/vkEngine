//
// Created by stiven on 17-10-20.
//

#include <vkEngine/manager/textureManager.h>
#include <vkEngine/vulkan/initVulkan.hpp>

namespace vkEngine{
     TextureManager TextureManager::textureManager;

    int TextureManager::get(std::string &&path) {

	    auto res = textureManager.textures.get(std::move(path), [](std::string const & path, Texture & text){
	        text = vkEngine::InitVulkan::getInstance().createTexture(std::move(path));
        });
	    vkEngine::InitVulkan::getInstance().updateDescriptorTexture();
	    return res;
    }

    const TextureManager::Texture & TextureManager::get(int i) const{
        return textureManager.textures[i];
    }

    void TextureManager::use(int i, int numberTexture) {
        //textureManager.textures[i].bindTexture(numberTexture);
    }

    TextureManager::TextureManager() {
        //textures.add("empty", glish::Texture::makeEmptyTexture());
    }

	const AssetsList<TextureManager::Texture > &TextureManager::getAssetsList() {
		return textureManager.textures;
	}

	const TextureManager::Texture &TextureManager::last()  {
		return textureManager.textures.last();
	}
}