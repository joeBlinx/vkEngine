//
// Created by stiven on 17-10-02.
//

#include <vkEngine/scene/node.h>
#include <algorithm>
#include <inc/vkEngine/manager/assetsManager.h>
#include <vkEngine/vulkan/initVulkan.hpp>
#include <scene/rootNode.hpp>
namespace vkEngine {
    RootNode Node::root{glm::vec3{0, 0, 0}};

    void Node::addChild(Node *child) {
        childs.insert(child);
        child->parent = this;
	    incrementNumber();
	    vkEngine::InitVulkan::getInstance().needNewRecord();
    }

    void Node::removeChild(Node *child) {
        auto itEnd = std::end(childs);
        auto it = std::find(std::begin(childs), itEnd, child);
	    decrementNumber(child->size());
        if (it != itEnd) {
            childs.erase(it);
        }
	    vkEngine::InitVulkan::getInstance().needNewRecord();

    }

    Node::~Node() {

    }

    Node::Node(glm::vec3 const &pos) : MovableObject(pos) {

    }



    void Node::addToRoot(Node *child) {
        root.addChild(child);
    }

    void Node::removeFromRoot(Node *child) {
        root.removeChild(child);
    }

    Node::iterator Node::begin()  {
        return childs.begin();
    }

    Node::const_iterator Node::begin() const {
        return childs.cbegin();

    }

    Node::iterator Node::end()  {
        return childs.end();

    }

    Node::const_iterator Node::end() const {
        return childs.cend();

    }

    RootNode * Node::getRoot() {
        return &root;
    }

    void Node::addModel(std::string &&a) {
        model.meshId = AssetsManager::getModel(std::move(a));

    }
    void Node::addMaterial(std::string &&path) {
        model.materials = AssetsManager::getMaterials(std::move(path));
        for(auto &[key, value] : MaterialManager::get(model.materials)){
            model.textures.emplace_back(AssetsManager::getTexture(std::move(*value.diffuseTexturePath)));
        }
    }
    const model3d &Node::getModel() const {
        return model;
    }

    template<class Function>
    void rotateThing(Function func, Node * parent, Node * child){
        child->changeRotationCenter(*parent);
        func();
        child->lastRotationCenter();
    }

    void Node::rotateX(float anglex, Axis axisCoordinate) {
        for( auto child : childs){
            rotateThing([child, anglex]
                        {child->rotateX(anglex, Axis::WORLD);}, this, child);
        }
        MovableObject::rotateX(anglex, axisCoordinate);
    }

    void Node::rotateY(float angley, Axis axisCoordinate) {
        for( auto child : childs){
            rotateThing([child, angley]
                        {child->rotateY(angley, Axis::WORLD);}, this, child);
        }
        MovableObject::rotateY(angley, axisCoordinate);
    }

    void Node::rotateZ(float anglez, Axis axisCoordinate) {
        for( auto child : childs){
            rotateThing([child, anglez]
                        {child->rotateZ(anglez, Axis::WORLD);}, this, child);

        }
        MovableObject::rotateZ(anglez, axisCoordinate);
    }



    void Node::moveX(float deltax, Axis axisCoordinate) {
        for( auto child : childs){
            child->moveX(deltax, Axis::PARENT);
        }
        MovableObject::move(deltax, chooseCoordinate(axisCoordinate).xAxis);
    }

    void Node::moveY(float deltay, Axis axisCoordinate) {
        for( auto child : childs){
            child->moveY(deltay, Axis::PARENT);
        }
        MovableObject::move(deltay, chooseCoordinate(axisCoordinate).yAxis);
    }

    void Node::moveZ(float deltaz, Axis axisCoordinate) {
        for( auto child : childs){
            child->moveZ(deltaz, Axis::PARENT);
        }
        MovableObject::move(deltaz, chooseCoordinate(axisCoordinate).zAxis);
    }
    axis Node::chooseCoordinate(Axis axisCoordinate) {
        switch (axisCoordinate){

            case Axis ::OWN :
                return axe;
            case Axis::WORLD:
                return worldAxis;
            case Axis::PARENT:
                return parent->getAxis();
        }
    }

	void Node::incrementNumber() {
		number++;
		if(parent){
			parent->incrementNumber();
		}

	}

	void Node::decrementNumber(int numberToRemove) {
		number-=numberToRemove;
		if(parent){
			parent->decrementNumber(numberToRemove);
		}
	}

    Node * Node::getRootAsNode() {
        return &root;
    }

	unsigned int Node::getId() const {
		return id;
	}

}
